import FRIENDSHIP from './friends/friendshipStatus';
import ADD_FRIEND_STATUS from './friends/addFriendStatus';
import GEOFENCES_LIST_STATUS from './geofences/geofencesListStatus';
import NOTIFICATIONS from './notifications/notificationsStatus';

//url for websockets
const WEB_SOCKET_URL = 'ws://138.197.213.190/cable?SESSION-TOKEN=';

export { FRIENDSHIP, ADD_FRIEND_STATUS, WEB_SOCKET_URL, GEOFENCES_LIST_STATUS, NOTIFICATIONS };

const ADD_FRIEND_STATUS = {
  SUCCESS: 'Заявка отправлена',
  ERROR: 'Ошибка. Попробуйте снова!'
};

export default ADD_FRIEND_STATUS;

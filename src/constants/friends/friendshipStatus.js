const FRIENDSHIP = {
    AWAITING_CONFIRMATION: 'Ожидает вашего подтверждения',
    REQUEST_SENT: 'Заявка отправлена',

    AWAITING_CONFIRMATION_FULL: 'хочет с вами подружиться!',
    REQUEST_SENT_FULL: 'еще не подтвердил дружбу с вами :(',
    NO_FRIENDSHIP_FULL: 'не в списке ваших друзей.'
}

export default FRIENDSHIP;
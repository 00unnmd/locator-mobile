import { DEFAULT_TEXT, RESPONSES, customAxios, setToken } from '../../axiosConfig/axiosConfig';

const signupRequest = async(form, confirmPassword, onSuccess, onError, onUsernameBusy,
                            onPhoneNumberBusy, onPasswordsNotMatch) => {

    if (form.password !== confirmPassword) { //passwords not confirmed
        onPasswordsNotMatch(DEFAULT_TEXT.PASSWORDS_NOT_MATCH);
    }
    else {
        await customAxios.post('/auth/register', form)

        .then( async(response) => {
            const SERVER_RESPONSE = response.data.result;
            console.log(response)
            //ok response
            if (SERVER_RESPONSE == RESPONSES.OK) {
                //save token after 200 from server and render Navigation
                await setToken(response.data.data.token);
                onSuccess(form);
            };
            //validation failed response
            if (SERVER_RESPONSE == RESPONSES.VALIDATION_FAILED) {
                if(response.data.data.field == 'number') {
                    onPhoneNumberBusy(DEFAULT_TEXT.PHONE_NUMBER_BUSY);
                };
                if(response.data.data.field == 'username') {
                    onUsernameBusy(DEFAULT_TEXT.LOGIN_BUSY)
                };
            };
            //wrong_format response
            if (SERVER_RESPONSE == RESPONSES.WRONG_FORMAT) {
                onError(DEFAULT_TEXT.NOT_ALL_DATA);
            };
        });
    };
};

export default signupRequest;
import { customAxios, DEFAULT_TEXT, RESPONSES, setToken } from '../../axiosConfig/axiosConfig';

const loginRequest = async(form, onSuccess, onError) => {
    await customAxios.post('/auth/login', form)
    .then(async(response) => {
        const SERVER_RESPONSE = response.data.result;

        if (SERVER_RESPONSE == RESPONSES.OK) {
            //save token after 200 from server and render Navigation
            await setToken(response.data.data.token);
            onSuccess(form);
        }
        //wrong_data response
        if (SERVER_RESPONSE == RESPONSES.WRONG_DATA) {
            onError(DEFAULT_TEXT.INCORRECT_LOGIN_OR_PASSWORD)
        };
        //wrong_format response
        if (SERVER_RESPONSE == RESPONSES.WRONG_FORMAT) {
            onError(DEFAULT_TEXT.NOT_ALL_DATA);
        };
    });
};

export default loginRequest;
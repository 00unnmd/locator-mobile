import { customAxios, getToken, RESPONSES, DEFAULT_TEXT } from '../../axiosConfig/axiosConfig';

const createRouteRequest = async(user, period, onSuccess, onError) => {
  const token = await getToken();
  const config = {
    headers: { 'SESSION-TOKEN': token },
    params: {
      from: period.from,
      to: period.to
    }
  };

  await customAxios.get(`/tracks/${user.id}`, config).then((response) => {
    const SERVER_RESPONSE = response.data.result;

    switch (SERVER_RESPONSE) {
      case RESPONSES.OK: {
        if (response.data.data.length < 2) {
          return onError(DEFAULT_TEXT.ROUTE_EMPTY)
        }
        else {
          return onSuccess(response.data.data);
        }
      }
      case RESPONSES.WRONG_ID: {
        return onError(DEFAULT_TEXT.WRONG_ID);
      }
      case RESPONSES.WRONG_SESSION_TOKEN: {
        return onError(DEFAULT_TEXT.WRONG_SESSION_TOKEN)
      }
    }
  })
};

export default createRouteRequest;
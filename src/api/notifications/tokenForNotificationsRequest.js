import { customAxios, getToken, RESPONSES } from '../axiosConfig/axiosConfig';

const tokenForNotificationsRequest = async (notificationToken, onSuccess, onError) => {
  const token = await getToken();
  const config = {
    headers: {'SESSION-TOKEN': token}
  };

  await customAxios.post('/token', notificationToken, config).then(response => {
    const SERVER_RESPONSE = response.data.result;

    switch (SERVER_RESPONSE) {
      case RESPONSES.OK: {
        return onSuccess();
      }
      case RESPONSES.WRONG_FORMAT: {
        return onError();
      }
      case RESPONSES.WRONG_SESSION_TOKEN: {
        return onError();
      }
    }
  });
};

export default tokenForNotificationsRequest;
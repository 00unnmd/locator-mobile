import axios from 'axios';
import * as SecureStore from 'expo-secure-store';

//for save images in base64 format
const BASE64 = 'data:image/jpeg;base64,';

//mesagges about incorrect data for users
const DEFAULT_TEXT = {
  NOT_ALL_DATA: 'Введены не все данные!',
  INCORRECT_LOGIN_OR_PASSWORD: 'Неверно введен логин или пароль!',
  PASSWORDS_NOT_MATCH: 'Пароли не совпадают!',
  PHONE_NUMBER_BUSY: 'Номер телефона уже используется!',
  LOGIN_BUSY: 'Логин уже используется!',

  WRONG_SESSION_TOKEN: 'Ошибка авторизации. Пожалуйста, перезайдите в приложение.',
  WRONG_ID: 'Неверный ID пользователя. Попробуйте снова!',
  WRONG_FORMAT: 'Какие-то данные введены неверно. Попробуйте снова!',
  SEARCH_FIELD_EMPTY: 'Некорректный запрос. Попробуйте снова!',
  SEARCH_RESPONSE_EMPTY: 'По данному запросу мы никого не нашли. :(',
  ROUTE_EMPTY: 'Нет данных по маршруту пользователя за этот промежуток времени.'
};

//processed responses from server
const RESPONSES = {
  OK: 'ok',

  //auth
  VALIDATION_FAILED: 'validation_failed',
  WRONG_DATA: 'wrong_data',
  WRONG_FORMAT: 'wrong_format',

  WRONG_SESSION_TOKEN: 'wrong_session_token',

  //friends
  WRONG_ID: 'wrong_id',
};

const customAxios = axios.create({
  baseURL: 'http://138.197.213.190/api/v1/',
});

const setToken = async token => {
  return await SecureStore.setItemAsync('loginToken', token);
};

const getToken = async () => {
  // return await SecureStore.getItemAsync('loginToken');
  let token = await SecureStore.getItemAsync('loginToken');
  return token;
};

export { BASE64, DEFAULT_TEXT, RESPONSES, customAxios, getToken, setToken };

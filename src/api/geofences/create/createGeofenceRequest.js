import { customAxios, getToken, RESPONSES, DEFAULT_TEXT } from '../../axiosConfig/axiosConfig';

const createGeofenceRequest = async (user, geofence, onSuccess, onError) => {
  const token = await getToken();
  const config = {
    headers: { 'SESSION-TOKEN': token },
  };

  await customAxios.post(`/${user.id}/zones`, geofence, config).then(response => {
    const SERVER_RESPONSE = response.data.result;

    switch (SERVER_RESPONSE) {
      case RESPONSES.OK: {
        return onSuccess();
      }
      case RESPONSES.WRONG_FORMAT: {
        return onError(DEFAULT_TEXT.WRONG_FORMAT);
      }
      case RESPONSES.WRONG_ID: {
        return onError(DEFAULT_TEXT.WRONG_ID);
      }
      case RESPONSES.WRONG_SESSION_TOKEN: {
        return onError(DEFAULT_TEXT.WRONG_SESSION_TOKEN);
      }
    }
  });
};

export default createGeofenceRequest;

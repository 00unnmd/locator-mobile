import { customAxios, RESPONSES, DEFAULT_TEXT, getToken } from '../../axiosConfig/axiosConfig';

const geofencesListRequest = async (user, onSuccess, onError) => {
  const token = await getToken();
  const config = {
    headers: { 'SESSION-TOKEN': token },
  };

  await customAxios.get(`/${user.id}/zones`, config).then(response => {
    const SERVER_RESPONSE = response.data.result;

    switch (SERVER_RESPONSE) {
      case RESPONSES.OK: {
        return onSuccess(response.data.data);
      }
      case RESPONSES.WRONG_ID: {
        return onError(DEFAULT_TEXT.WRONG_ID);
      }
      case RESPONSES.WRONG_SESSION_TOKEN: {
        return onError(DEFAULT_TEXT.WRONG_SESSION_TOKEN);
      }
    }
  });
};

export default geofencesListRequest;

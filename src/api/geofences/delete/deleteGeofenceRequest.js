import { customAxios, RESPONSES, DEFAULT_TEXT, getToken } from '../../axiosConfig/axiosConfig';

const deleteGeofenceRequest = async (user, zone_id, onSuccess, onError) => {
  const token = await getToken();
  const config = {
    headers: { 'SESSION-TOKEN': token },
    params: { zone_id }
  };

  await customAxios.delete(`/${user.id}/zones`, config).then(response => {
    const SERVER_RESPONSE = response.data.result;
    console.log(SERVER_RESPONSE);

    switch (SERVER_RESPONSE) {
      case RESPONSES.OK: {
        return onSuccess();
      }
      case RESPONSES.WRONG_ID: {
        return onError(DEFAULT_TEXT.WRONG_ID);
      }
      case RESPONSES.WRONG_SESSION_TOKEN: {
        return onError(DEFAULT_TEXT.WRONG_SESSION_TOKEN);
      }
    }
  });
};

export default deleteGeofenceRequest;
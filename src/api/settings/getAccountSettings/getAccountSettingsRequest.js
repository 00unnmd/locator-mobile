import { customAxios, getToken, RESPONSES, DEFAULT_TEXT } from '../../axiosConfig/axiosConfig';

const getAccountSettingsRequest = async (onSuccess, onError) => {
  const token = await getToken();
  const config = {
    headers: { 'SESSION-TOKEN': token },
  };

  await customAxios.get('settings', config).then(response => {
    const SERVER_RESPONSE = response.data.result;
    console.log(response);

    switch (SERVER_RESPONSE) {
      case RESPONSES.OK: {
        return onSuccess(response.data.data);
      }
      case RESPONSES.WRONG_SESSION_TOKEN: {
        return onError(DEFAULT_TEXT.WRONG_SESSION_TOKEN);
      }
    }
  })
};

export default getAccountSettingsRequest;
import { customAxios, DEFAULT_TEXT, RESPONSES, getToken } from '../../axiosConfig/axiosConfig';

const deleteFriendRequest = async(user_id, onSuccess, onError) => {
    const token = await getToken();
    const config = {
        headers: {'SESSION-TOKEN': token}
    };

    await customAxios.delete(`friends/${user_id}`, config)
    .then((response) => {
        const SERVER_RESPONSE = response.data.result;

        switch (SERVER_RESPONSE) {
            case RESPONSES.OK: {
                onSuccess();
            };
            case RESPONSES.WRONG_ID: {
                onError();
            };
            case RESPONSES.WRONG_SESSION_TOKEN: {
                onError();
            };
        };
    });
};

export default deleteFriendRequest;
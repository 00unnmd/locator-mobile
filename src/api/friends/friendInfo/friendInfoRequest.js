import { customAxios, DEFAULT_TEXT, RESPONSES, getToken } from '../../axiosConfig/axiosConfig';

const friendInfoRequest = async (user, onSuccess, onError) => {
  const token = await getToken();
  const config = {
    headers: { 'SESSION-TOKEN': token },
  };

  await customAxios.get(`friends/${user.id}`, config).then(response => {
    const SERVER_RESPONSE = response.data.result;
    //console.log(response);

    if (SERVER_RESPONSE == RESPONSES.OK) {
      onSuccess(response.data.data);
    }
    if (SERVER_RESPONSE == RESPONSES.WRONG_ID) {
      onError(DEFAULT_TEXT.WRONG_ID);
    }
    if (SERVER_RESPONSE == RESPONSES.WRONG_SESSION_TOKEN) {
      onError(DEFAULT_TEXT.WRONG_SESSION_TOKEN);
    }
  });
};

export default friendInfoRequest;

import { customAxios, DEFAULT_TEXT, RESPONSES, getToken } from '../../axiosConfig/axiosConfig';

const searchFriendsRequest = async (search_name, onSuccess, onError) => {
	const token = await getToken();
	const config = {
		headers: { 'SESSION-TOKEN': token },
		params: { search_name }
	};

	await customAxios.get('friends/find', config)
		.then((response) => {
			const SERVER_RESPONSE = response.data.result;

			if (SERVER_RESPONSE == RESPONSES.OK) {
				if (response.data.data.length === 0) {
					onError(DEFAULT_TEXT.SEARCH_RESPONSE_EMPTY)
				}
				else {
					onSuccess(response.data.data);
				}
			};
			if (SERVER_RESPONSE == RESPONSES.WRONG_FORMAT) {
				onError(DEFAULT_TEXT.SEARCH_FIELD_EMPTY);
			};
			if (SERVER_RESPONSE == RESPONSES.WRONG_SESSION_TOKEN) {
				onError(DEFAULT_TEXT.WRONG_SESSION_TOKEN);
			};
		});
};

export default searchFriendsRequest;
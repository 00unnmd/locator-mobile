import { customAxios, DEFAULT_TEXT, RESPONSES, getToken } from '../../axiosConfig/axiosConfig';

const replyFriendRequest = async (user_id, verified, onSuccess, onError) => {
  const token = await getToken();
  const data = {
    verified: verified.toString()
  };
  const config = {
    headers: { 'SESSION-TOKEN': token }
  };

  await customAxios.post(`friends/${user_id}/response`, data, config).then(response => {
    const SERVER_RESPONSE = response.data.result;

    switch (SERVER_RESPONSE) {
      case RESPONSES.OK: {
        onSuccess();
      }
      case RESPONSES.WRONG_ID: {
        onError();
      }
      case RESPONSES.WRONG_SESSION_TOKEN: {
        onError();
      }
    }
  });
};

export default replyFriendRequest;

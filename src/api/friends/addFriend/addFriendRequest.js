import { customAxios, RESPONSES, getToken } from '../../axiosConfig/axiosConfig';
import { ADD_FRIEND_STATUS } from '../../../constants/constants';

const addFriendRequest = async (user_id, onSuccess, onError) => {
  const token = await getToken();
  const config = {
    headers: { 'SESSION-TOKEN': token },
  };

  await customAxios.post(`friends/${user_id}/request`, null, config).then((response) => {
    console.log(response);
    const SERVER_RESPONSE = response.data.result;

    switch (SERVER_RESPONSE) {
      case RESPONSES.OK: {
        return onSuccess(ADD_FRIEND_STATUS.SUCCESS);
      }
      case RESPONSES.WRONG_ID: {
        return onError(ADD_FRIEND_STATUS.ERROR);
      }
      case RESPONSES.WRONG_SESSION_TOKEN: {
        return onError(ADD_FRIEND_STATUS.ERROR);
      }
    }
  });
};

export default addFriendRequest;

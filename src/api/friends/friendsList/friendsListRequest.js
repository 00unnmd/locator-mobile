import { customAxios, DEFAULT_TEXT, RESPONSES, getToken } from '../../axiosConfig/axiosConfig';

const friendsListRequest = async (onSuccess, onError) => {
  const token = await getToken();
  const config = {
    headers: { 'SESSION-TOKEN': token },
  };

  await customAxios.get('friends', config).then(response => {
    const SERVER_RESPONSE = response.data.result;

    if (SERVER_RESPONSE == RESPONSES.OK) {
      onSuccess(response.data.data);
    }

    if (SERVER_RESPONSE == RESPONSES.WRONG_SESSION_TOKEN) {
      onError(DEFAULT_TEXT.WRONG_SESSION_TOKEN);
    }
  });
};

export default friendsListRequest;

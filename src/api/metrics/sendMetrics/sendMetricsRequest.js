import { customAxios, DEFAULT_TEXT, RESPONSES, getToken } from '../../axiosConfig/axiosConfig';

const sendMetricsRequest = async (data, onSuccess, onError) => {
  const token = await getToken();
  const config = {
    headers: { 'SESSION-TOKEN': token },
  };

  await customAxios.post('/metrics', data, config).then((response) => {
    console.log(response);
  });
};

export default sendMetricsRequest;

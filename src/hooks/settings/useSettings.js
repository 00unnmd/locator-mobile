import { useSelector, useDispatch } from 'react-redux';
import { accountSettingsSaver, accountSettingsError, modalVisibilityToggler, dropData } from '../../store/settings/actions';
import { clearStorage } from '../../store/actions'
import getAccountSettingsRequest from '../../api/settings/getAccountSettings/getAccountSettingsRequest';

const useSettings = () => {
  const { settings, settingsTextResponse, settingsIsReady, modalVisibility } = useSelector(state => ({
    settings: state.settings.settings,
    settingsTextResponse: state.settings.settingsTextResponse,
    settingsIsReady: state.settings.settingsIsReady,
    modalVisibility: state.settings.modalVisibility
  }));
  const dispatch = useDispatch();

  const showSettingsModal = () => {
    dispatch(modalVisibilityToggler(true));
  }
  const closeSettingsModal = () => {
    dispatch(modalVisibilityToggler(false));
  }
  const dropSettings = () => {
    dispatch(dropData());
  }
  const logoutClearing = () => {
    dispatch(clearStorage());
  }

  const accountSettingsRequestProcessing = async () => {
    const onSuccess = success => {
      dispatch(accountSettingsSaver(success));
    }
    const onError = error => {
      dispatch(accountSettingsError(error));
    }

    try {
      await getAccountSettingsRequest(onSuccess, onError);
    }
    catch (error) {
      console.log(error);
    }
  };

  return {
    settings,
    settingsTextResponse,
    settingsIsReady,
    modalVisibility,
    accountSettingsRequestProcessing,
    showSettingsModal,
    closeSettingsModal,
    dropSettings,
    logoutClearing
  };
};

export default useSettings;
import { useSelector, useDispatch } from 'react-redux';

import { mapRefSaver } from '../../store/map/actions';

const useMapRef = () => {
  const { mapRef } = useSelector(state => ({
    mapRef: state.mapRef.mapRef,
  }));
  const dispatch = useDispatch();

  const saveMapRef = map => {
    dispatch(mapRefSaver(map));
  };
  return {
    mapRef,
    saveMapRef,
  };
};

export default useMapRef;

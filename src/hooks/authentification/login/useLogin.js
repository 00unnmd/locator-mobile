import { useSelector, useDispatch } from 'react-redux';

import {
  loginSuccess,
  loginError,
  setDefault,
} from '../../../store/authentification/login/actions';
import { setLoginStatus } from '../../../store/authentification/login-wrap/actions';
import { setTokenConfirmation } from '../../../store/authentification/login-control/actions';

import loginRequest from '../../../api/authentification/login/loginRequest';

const useLogin = () => {
  const { user, incorrectData, textResponse } = useSelector((state) => ({
    user: state.login.user,
    incorrectData: state.login.incorrectData,
    textResponse: state.login.textResponse,
  }));
  const dispatch = useDispatch();

  const onStatusChange = (value) => {
    dispatch(setLoginStatus(value));
  };
  const clearFields = () => {
    dispatch(setDefault());
  };

  const loginRequestProcessing = async (form) => {
    //event when submit button is clicked
    const onSuccess = (success) => {
      dispatch(loginSuccess(success));
      dispatch(setTokenConfirmation(true));
    };
    const onError = (error) => {
      dispatch(loginError(error));
    };

    try {
      await loginRequest(form, onSuccess, onError);
    } catch (error) {
      console.log(error);
    }
  };

  return { user, incorrectData, textResponse, onStatusChange, clearFields, loginRequestProcessing };
};

export default useLogin;

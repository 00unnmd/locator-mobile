import { useSelector, useDispatch } from 'react-redux';

import {
        signupSuccess,
        signupError,
        setDefault,
        passwordsNotMatch,
        usernameBusy,
        phoneNumberBusy
} from '../../../store/authentification/signup/actions';
import { setLoginStatus } from '../../../store/authentification/login-wrap/actions';
import { setTokenConfirmation } from '../../../store/authentification/login-control/actions';

import signupRequest from '../../../api/authentification/signup/signupRequest';

const useSignup = () => {
    const { badNumber, badUsername, badPassword, incorrectData, textResponse } = useSelector((state) => ({
        badNumber: state.signup.badNumber,
        badUsername: state.signup.badUsername,
        badPassword: state.signup.badPassword,
        incorrectData: state.signup.incorrectData,
        textResponse: state.signup.textResponse,
    }));
    const dispatch = useDispatch();
    
    const onStatusChange = (value) => {
        dispatch(setLoginStatus(value));
    };
    const clearFields = () => {
        dispatch(setDefault());
    };

    const signupRequestProcessing = async(form, confirmPassword) => {  //event when submit button is clicked
        const onSuccess = (success) => {
            dispatch(signupSuccess(success));
            dispatch(setTokenConfirmation(true));
        };
        const onError = (error) => {
            dispatch(signupError(error));
        };
        const onUsernameBusy =(response) => {
            dispatch(usernameBusy(response));
        };
        const onPhoneNumberBusy = (response) => {
            dispatch(phoneNumberBusy(response));
        };
        const onPasswordsNotMatch = (response) => {
            dispatch(passwordsNotMatch(response));
        };

        try {
            await signupRequest(form, confirmPassword, onSuccess, onError,
                                onUsernameBusy, onPhoneNumberBusy, onPasswordsNotMatch);
        }
        catch (error) {
            console.log(error);
        };
    };

    return { badNumber, badUsername, badPassword, incorrectData, textResponse,
             clearFields, onStatusChange, signupRequestProcessing };
};

export default useSignup;
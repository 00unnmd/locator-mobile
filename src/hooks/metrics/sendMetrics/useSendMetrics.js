import { Alert, Linking } from 'react-native';

import * as Location from 'expo-location';
import * as TaskManager from 'expo-task-manager';
import * as Battery from 'expo-battery';
import moment from 'moment';

const LOCATION_BACKGROUND_UPDATE = 'background-location-task';

const useSendMetrics = () => {
  const getLocationPremissions = async () => {
    let locationPremissionStatus = await Location.getPermissionsAsync();

    console.log(locationPremissionStatus)
    //premissions not granted
    if (locationPremissionStatus.status != 'granted') {
      //ask user for premissions
      if (locationPremissionStatus.canAskAgain) {
        locationPremissionStatus = await Location.requestPermissionsAsync();
        //if i cant ask user
      } else {
        Alert.alert(
          'Мы не знаем где вы!',
          'Чтобы использовать все возможности приложения, разрешите доступ к геолокации в настройках!',
          [
            {
              text: 'Продолжить без геолокации',
              style: 'destructive',
            },
            {
              text: 'В настройки',
              onPress: () => {
                Linking.openURL('app-settings:');
              },
              style: 'cancel',
            },
          ]
        );
      }
    }
    //premissions granted
    if (locationPremissionStatus.status === 'granted') {
      console.log('granted');
      await Location.startLocationUpdatesAsync(LOCATION_BACKGROUND_UPDATE, {
        accuracy: Location.Accuracy.High,
        distanceInterval: 10,
        showsBackgroundLocationIndicator: true,
      }).then(() => {
        console.log('started location updates');
      });
    }
  };

  return { getLocationPremissions };
};

const getCurrentDateTime = () => {
  return moment().format();
};
const getCurrentBatteryLevel = async () => {
  let batteryLevel = await Battery.getBatteryLevelAsync();
  return Math.trunc(batteryLevel * 100);
};

const sendMetrics = async (location) => {
  const battery = await getCurrentBatteryLevel();
  const datetime = getCurrentDateTime();
  const data = { battery, datetime, location };
  console.log(data);
  //place for sendMetrics request
};

TaskManager.defineTask(LOCATION_BACKGROUND_UPDATE, ({ data, error }) => {
  if (data) {
    //const { location } = data;
    const location = {
      longitude: 45.6854,
      latitude: 137.3357,
    };
    sendMetrics();
  }
  if (error) {
    console.log(error);
  }
});

export default useSendMetrics;

import { Platform } from 'react-native';
import { Notifications } from 'expo';
import * as Permissions from 'expo-permissions';
import { NOTIFICATIONS } from '../../../constants/constants';

import tokenForNotificationsRequest from '../../../api/notifications/tokenForNotificationsRequest';

const useNotifications = () => {

  const registerForPushNotificationsAsync = async () => {
    const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS);

    console.log(`Notifications Permissions status: ${status}`);
    if (status !== 'granted') {
      alert(`${NOTIFICATIONS.DISABLE}`);
    }
    else {
      const fullToken = await Notifications.getExpoPushTokenAsync();
      //обрезаю название токена(в запрос идут только символы внутри квадратных скобок)
      const token = fullToken.substring(fullToken.lastIndexOf('[') + 1, fullToken.length - 1);
      const os = Platform.OS;
      const notificationsToken = { token, os };

      tokenForNotificationsRequestProcessing(notificationsToken);
    }
  };

  const tokenForNotificationsRequestProcessing = async (notificationsToken) => {
    const onSuccess = success => {
      console.log(success);
    };
    const onError = error => {
      console.log(error);
    };

    try {
      await tokenForNotificationsRequest(notificationsToken ,onSuccess, onError);
    }
    catch(error) {
      console.log(error)
    }
  };

  return { registerForPushNotificationsAsync };
};

export default useNotifications;
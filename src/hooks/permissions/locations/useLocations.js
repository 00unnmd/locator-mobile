import * as Permissions from 'expo-permissions';

const useLocations = () => {
    const askForLocationPermissionsAsync = async () => {
        const { status } = await Permissions.askAsync(Permissions.LOCATION)

        console.log(`Locations Permissions status: ${status}`)
        if (status !== 'granted') {
            alert('Ваши друзья не знают, где Вы! Советуем включить передачу геолокации в настройках.');
        }
    };

    return { askForLocationPermissionsAsync };
};

export default useLocations;
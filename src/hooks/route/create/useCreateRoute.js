import { useSelector, useDispatch } from 'react-redux';
import moment from 'moment';

import {
  createRouteToggler,
  createRouteSaver,
  createRouteError,
  resetRoute,
} from '../../../store/route/create/actions';

import createRouteRequest from '../../../api/route/createRoute/createRouteRequest';
import randomColorGenerator from '../../../helpers/randomColorGenerator';

const useCreateRoute = () => {
  const {
    createRouteIsVisible,
    routeCoordinates,
    strokeColors,
    textResponse,
    routeCoordinatesIsReady,
  } = useSelector(state => ({
    createRouteIsVisible: state.createRoute.createRouteIsVisible,
    routeCoordinates: state.createRoute.routeCoordinates,
    strokeColors: state.createRoute.strokeColors,
    textResponse: state.createRoute.textResponse,
    routeCoordinatesIsReady: state.createRoute.routeCoordinatesIsReady,
  }));
  const dispatch = useDispatch();

  const showCreateRoute = () => {
    dispatch(createRouteToggler(true));
  };
  const closeCreateRoute = () => {
    dispatch(createRouteToggler(false));
  };

  const errorCreateRoute = error => {
    dispatch(createRouteError(error));
  };
  const resetRouteData = () => {
    dispatch(resetRoute());
  };

  const createRouteRequestProcessing = async (user, period) => {
    resetRouteData();
    const onSuccess = async success => {
      let strokeColors = await success.map(randomColorGenerator);
      let updatedSuccess = await success.map((item) => {
        let fullDate = moment(item.timestamp).format('DD.MM.YYYY HH:mm').split(' ');
        item.timestamp = {
          date: fullDate[0],
          time: fullDate[1],
        };
        return item;
      });

      dispatch(createRouteSaver(updatedSuccess, strokeColors));
    };
    const onError = errorMessage => {
      dispatch(createRouteError(errorMessage));
    };

    try {
      await createRouteRequest(user, period, onSuccess, onError);
    } catch (error) {
      console.log(error);
    }
  };

  return {
    createRouteIsVisible,
    routeCoordinates,
    strokeColors,
    textResponse,
    routeCoordinatesIsReady,
    showCreateRoute,
    closeCreateRoute,
    createRouteRequestProcessing,
    errorCreateRoute,
  };
};

export default useCreateRoute;

import { useSelector, useDispatch } from 'react-redux';

import {
  friendsListSuccess,
  friendsListWrongSessionToken,
  friendsDrawerToggler,
} from '../../../store/friends/list/actions';
import friendsListRequest from '../../../api/friends/friendsList/friendsListRequest';

const useFriendsList = () => {
  const { friendsList, friendsListIsReady, listTextResponse, friendsDrawerVisible } = useSelector(
    state => ({
      friendsList: state.friendsList.friendsList,
      friendsListIsReady: state.friendsList.friendsListIsReady,
      listTextResponse: state.friendsList.listTextResponse,
      friendsDrawerVisible: state.friendsList.friendsDrawerVisible,
    })
  );
  const dispatch = useDispatch();

  const showFriendListDrawer = () => {
    dispatch(friendsDrawerToggler(true));
  };
  const closeFriendListDrawer = () => {
    dispatch(friendsDrawerToggler(false));
  };

  const friendsListRequestProcessing = async () => {
    const onSuccess = success => {
      dispatch(friendsListSuccess(success));
    };
    const onError = error => {
      dispatch(friendsListWrongSessionToken(error));
    };

    try {
      await friendsListRequest(onSuccess, onError);
    } catch (error) {
      console.log(error);
    }
  };

  return {
    friendsList,
    friendsListIsReady,
    listTextResponse,
    friendsDrawerVisible,
    showFriendListDrawer,
    closeFriendListDrawer,
    friendsListRequestProcessing,
  };
};

export default useFriendsList;

import replyFriendRequest from '../../../api/friends/replyFriend/replyFriendRequest';

import useFriendsList from '../friendsList/useFriendsList';
import useFriendInfo from '../../../hooks/friends/friendInfo/useFriendInfo';

const useReplyFriend = () => {
  const { clearUserInfo } = useFriendInfo();
  const { showFriendListDrawer } = useFriendsList();

  const onSuccess = success => {
    clearUserInfo();
    showFriendListDrawer();
  };
  const onError = error => {
    clearUserInfo();
    showFriendListDrawer();
  };

  const applyFriendRequestProcessing = async user => {
    try {
      await replyFriendRequest(user.id, true, onSuccess, onError);
    } catch (error) {
      console.log(error);
    }
  };
  const declineFriendRequestProcessing = async user => {
    try {
      await replyFriendRequest(user.id, false, onSuccess, onError);
    } catch (error) {
      console.log(error);
    }
  };

  return { applyFriendRequestProcessing, declineFriendRequestProcessing };
};

export default useReplyFriend;

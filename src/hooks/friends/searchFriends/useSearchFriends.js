import { useSelector, useDispatch } from 'react-redux';

import {
  toggleSearchListVisibility,
  saveInputValue,
  searchFriendsSuccess,
  searchFriendsError,
  setDefaultValues,
  toggleLoadingStatus,
} from '../../../store/friends/search/actions';
import searchFriendsRequest from '../../../api/friends/searchFriends/searchFriendsRequest';

const useSearchFriends = () => {
  const {
    searchResultList,
    searchListIsVisible,
    searchResultIsReady,
    searchInputValue,
    loadingProcessing,
    searchTextResponse,
  } = useSelector((state) => ({
    searchResultList: state.searchFriends.searchResultList,
    searchListIsVisible: state.searchFriends.searchListIsVisible,
    searchResultIsReady: state.searchFriends.searchResultIsReady,
    searchInputValue: state.searchFriends.searchInputValue,
    loadingProcessing: state.searchFriends.loadingProcessing,
    searchTextResponse: state.searchFriends.searchTextResponse,
  }));
  const dispatch = useDispatch();

  const toggleSearchList = (visibility) => {
    dispatch(toggleSearchListVisibility(visibility));
  };

  const setDefault = async () => {
    dispatch(setDefaultValues());
  };
  const saveSearchInputValue = async (value) => {
    dispatch(saveInputValue(value));
    dispatch(toggleLoadingStatus(true));
  };

  const searchFriendsRequestProcessing = async (search_name) => {
    const onSuccess = (success) => {
      dispatch(searchFriendsSuccess(success));
    };
    const onError = (error) => {
      dispatch(searchFriendsError(error));
    };

    try {
      await setDefault();
      await saveSearchInputValue(search_name);
      await searchFriendsRequest(search_name, onSuccess, onError);
    } catch (error) {
      console.log(error);
    }
  };

  return {
    searchResultList,
    searchListIsVisible,
    searchResultIsReady,
    searchInputValue,
    loadingProcessing,
    searchTextResponse,
    searchFriendsRequestProcessing,
    toggleSearchList,
    setDefault,
  };
};

export default useSearchFriends;

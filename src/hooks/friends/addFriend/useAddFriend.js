import { useSelector, useDispatch } from 'react-redux';
import { friendInfoDrawerToggler } from '../../../store/friends/info/actions';
import { changeButtonTitle } from '../../../store/friends/add/actions';

import addFriendRequest from '../../../api/friends/addFriend/addFriendRequest';

const useAddFriend = () => {
  const { addFriendTextResponse } = useSelector(state => ({
    addFriendTextResponse: state.addFriend.addFriendTextResponse
  }));
  const dispatch = useDispatch();

  const addFriendRequestProcessing = async user_id => {
    const onSuccess = success => {
      dispatch(changeButtonTitle(success));
    };
    const onError = error => {
      dispatch(changeButtonTitle(error));
    };

    try {
      await addFriendRequest(user_id, onSuccess, onError);
    } catch (error) {
      console.log(error);
    }
  };

  return { addFriendTextResponse, addFriendRequestProcessing };
};

export default useAddFriend;

import deleteFriendRequest from '../../../api/friends/deleteFriend/deleteFriendRequest';
import useFriendInfo from '../../../hooks/friends/friendInfo/useFriendInfo';

const useDeleteFriend = () => {
  const { toggleFriendInfoDrawer } = useFriendInfo();
  const deleteFriendRequestProcessing = async user_id => {
    const onSuccess = success => {
      toggleFriendInfoDrawer(false);
    };
    const onError = error => {
      toggleFriendInfoDrawer(false);
    };

    try {
      await deleteFriendRequest(user_id, onSuccess, onError);
    } catch (error) {
      console.log(error);
    }
  };

  return { deleteFriendRequestProcessing };
};

export default useDeleteFriend;

import { useSelector, useDispatch } from 'react-redux';

import {
  userInfoSaver,
  verifiedFriendInfoSuccess,
  verifiedFriendInfoError,
  userInfoRemover,
} from '../../../store/friends/info/actions';
import friendInfoRequest from '../../../api/friends/friendInfo/friendInfoRequest';

const useFriendInfo = () => {
  const { user, friend, userInfoIsVisible, friendInfoIsReady, infoTextResponse } = useSelector(
    state => ({
      user: state.friendInfo.user,
      friend: state.friendInfo.friend,
      userInfoIsVisible: state.friendInfo.userInfoIsVisible,
      friendInfoIsReady: state.friendInfo.friendInfoIsReady,
      infoTextResponse: state.friendInfo.infoTextResponse,
    })
  );
  const dispatch = useDispatch();

  const showUserInfo = async user => {
    dispatch(userInfoSaver(user));
  };
  const clearUserInfo = () => {
    dispatch(userInfoRemover());
  };

  const friendInfoRequestProcessing = async user => {
    const onSuccess = success => {
      dispatch(verifiedFriendInfoSuccess(success));
    };
    const onError = error => {
      dispatch(verifiedFriendInfoError(error));
    };

    try {
      return await friendInfoRequest(user, onSuccess, onError);
    } catch (error) {
      console.log(error);
    }
  };

  return {
    user,
    friend,
    userInfoIsVisible,
    friendInfoIsReady,
    infoTextResponse,
    friendInfoRequestProcessing,
    showUserInfo,
    clearUserInfo,
  };
};

export default useFriendInfo;

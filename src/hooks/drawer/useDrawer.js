import { useSelector, useDispatch } from 'react-redux';

import { saveDrawerPosition } from '../../store/drawer/actions';

const useDrawer = () => {
  const { drawerExpanded } = useSelector(state => ({
    drawerExpanded: state.drawer.drawerExpanded
  }));
  const dispatch = useDispatch();

  //события ниже нужны для сохранения позиции drawer'а,
  //т.к. встроенных методов ".open()" и ".close()" в библиотеке нет
  //я сохраняю положение в redux
  const expand = () => {
    dispatch(saveDrawerPosition(true));
  };
  const collapse = () => {
    dispatch(saveDrawerPosition(false));
  };

  return { drawerExpanded, expand, collapse };
};

export default useDrawer;

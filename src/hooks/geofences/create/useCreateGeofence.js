import { useSelector, useDispatch } from 'react-redux';

import {
  createGeofenceToggler,
  centerCoordinatesSaver,
  changeGeofenceRadius,
  geofenceRadiusSaver,
  changeGeofenceName,
  changeGeofenceColor,
  saveNewGeofence,
  errorNewGeofence,
} from '../../../store/geofences/create/actions';
import createGeofenceRequest from '../../../api/geofences/create/createGeofenceRequest';

import useGeofencesList from '../list/useGeofencesList';

import useMapRef from '../../map/useMapRef';
import hexToRgbA from '../../../helpers/hexToRgba';

const useCreateGeofence = () => {
  const {
    createGeofenceIsReady,
    geofenceCenterIsReady,
    geofenceRadiusIsReady,
    geofenceNameIsReady,
    newGeofence,
    rgbaColor,
    createGeofenceTextResponse,
  } = useSelector(state => ({
    createGeofenceIsReady: state.createGeofence.createGeofenceIsReady,
    geofenceCenterIsReady: state.createGeofence.geofenceCenterIsReady,
    geofenceRadiusIsReady: state.createGeofence.geofenceRadiusIsReady,
    geofenceNameIsReady: state.createGeofence.geofenceNameIsReady,
    newGeofence: state.createGeofence.newGeofence,
    rgbaColor: state.createGeofence.rgbaColor,
    createGeofenceTextResponse: state.createGeofence.createGeofenceTextResponse,
  }));
  const dispatch = useDispatch();
  const { geofencesListRequestProcessing } = useGeofencesList();
  const { mapRef } = useMapRef();

  const showCreateGeofence = () => {
    dispatch(createGeofenceToggler(true));
  };
  const closeCreateGeofence = () => {
    dispatch(createGeofenceToggler(false));
  };

  const updateGeofenceRadius = radius => {
    dispatch(changeGeofenceRadius(radius));
  };
  const saveGeofenceRadius = () => {
    dispatch(geofenceRadiusSaver());
  };

  const updateGeofenceName = newName => {
    dispatch(changeGeofenceName(newName));
  };
  const updateGeofenceColor = async(newColor) => {
    let rgbaColor = await hexToRgbA(newColor);
    dispatch(changeGeofenceColor(newColor, rgbaColor));
  };

  const saveCenterCoordinates = async () => {
    let camera = await mapRef.getCamera();
    dispatch(centerCoordinatesSaver(camera.center));
  };

  const createNewGeofence = (user, geofence) => {
    createGeofenceRequestProcessing(user, geofence);
  };

  const createGeofenceRequestProcessing = async (user, geofence) => {
    const onSuccess = () => {
      dispatch(saveNewGeofence());
      geofencesListRequestProcessing(user);
    };
    const onError = error => {
      dispatch(errorNewGeofence(error));
      dispatch(saveNewGeofence());
    };

    try {
      await createGeofenceRequest(user, geofence, onSuccess, onError);
    } catch (error) {
      console.log(error);
    }
  };

  return {
    createGeofenceIsReady,
    geofenceCenterIsReady,
    geofenceRadiusIsReady,
    geofenceNameIsReady,
    newGeofence,
    rgbaColor,
    createGeofenceTextResponse,
    showCreateGeofence,
    closeCreateGeofence,
    updateGeofenceRadius,
    updateGeofenceName,
    updateGeofenceColor,
    saveCenterCoordinates,
    saveGeofenceRadius,
    createGeofenceRequestProcessing,
    createNewGeofence,
  };
};

export default useCreateGeofence;

import { useSelector, useDispatch } from 'react-redux';

import {
  geofencesListToggler,
  geofencesListSaver,
  geofencesListError,
  geofenceItemToggler,
} from '../../../store/geofences/list/actions';
import geofencesListRequest from '../../../api/geofences/list/geofencesListRequest';
import hexToRgbA from '../../../helpers/hexToRgba';

const useGeofencesList = () => {
  const {
    geofencesList,
    geofencesListIsReady,
    activeGeofenceCoordinates,
    geofencesListIsVisible,
    textResponse,
  } = useSelector(state => ({
    geofencesList: state.geofencesList.geofencesList,
    geofencesListIsReady: state.geofencesList.geofencesListIsReady,
    activeGeofenceCoordinates: state.geofencesList.activeGeofenceCoordinates,
    geofencesListIsVisible: state.geofencesList.geofencesListIsVisible,
    textResponse: state.geofencesList.textResponse,
  }));
  const dispatch = useDispatch();

  const showGeofenceList = () => {
    dispatch(geofencesListToggler(true));
  };
  const closeGeofenceList = () => {
    dispatch(geofencesListToggler(false));
  };

  const circleToCoordinates = (geofence) => {
    const earthRadius = 6378100; //meters
    const lat0 = geofence.center.latitude + (-geofence.radius / earthRadius) * (180 / Math.PI);
    const lat1 = geofence.center.latitude + (geofence.radius / earthRadius) * (180 / Math.PI);
    const lng0 = geofence.center.longitude + (-geofence.radius / earthRadius) * (180 / Math.PI) / Math.cos(geofence.center.latitude * Math.PI / 180);
    const lng1 = geofence.center.longitude + (geofence.radius / earthRadius) * (180 / Math.PI) / Math.cos(geofence.center.latitude * Math.PI / 180);

    return [{
      //top
      latitude: lat1,
      longitude: geofence.center.longitude
    }, {
      //left
      latitude: geofence.center.latitude,
      longitude: lng0
    }, {
      //right
      latitude: geofence.center.latitude,
      longitude: lng1
    }, {
      //bottom
      latitude: lat0,
      longitude: geofence.center.longitude
    },
    ]
  };
  const toggleGeofenceItem = geofence => {
    const coordinatesForCircle = circleToCoordinates(geofence);
    dispatch(geofenceItemToggler(coordinatesForCircle));
  };

  const geofencesListRequestProcessing = async user => {
    const onSuccess = async success => {
      await success.map(item => {
        item.color = hexToRgbA(item.color);
      });
      dispatch(geofencesListSaver(success));
    };
    const onError = error => {
      dispatch(geofencesListError(error));
    };

    try {
      await geofencesListRequest(user, onSuccess, onError);
    } catch (error) {
      console.log(error);
    }
  };

  return {
    geofencesList,
    geofencesListIsReady,
    activeGeofenceCoordinates,
    geofencesListIsVisible,
    textResponse,
    showGeofenceList,
    closeGeofenceList,
    geofencesListRequestProcessing,
    toggleGeofenceItem,
  };
};

export default useGeofencesList;

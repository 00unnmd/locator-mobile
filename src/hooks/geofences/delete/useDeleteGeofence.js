import deleteGeofenceRequest from '../../../api/geofences/delete/deleteGeofenceRequest';

import useGeofencesList from '../list/useGeofencesList';

const useDeleteGeofence = () => {
  const { geofencesListRequestProcessing } = useGeofencesList();

  const deleteGeofenceRequestProcessing = async(user, zone_id) => {
    let errorMessage;

    const onSuccess = () => {
      geofencesListRequestProcessing(user);
    };
    const onError = (error) => {
      return errorMessage = error;
    };

    try {
      await deleteGeofenceRequest(user, zone_id, onSuccess, onError);
    }
    catch(error) {
      console.log(error)
    }

    return errorMessage;
  };

  return { deleteGeofenceRequestProcessing };
};

export default useDeleteGeofence;
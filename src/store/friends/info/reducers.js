import moment from 'moment';
import 'moment/locale/ru';

import {
  SAVE_USER_INFO,
  FRIEND_INFO_SUCCESS,
  FRIEND_INFO_ERROR,
  FRIEND_INFO_UPDATE_DATA,
  FRIEND_INFO_CLEAR,
} from './actions';
import { FRIENDSHIP } from '../../../constants/constants';

const defaultState = {
  user: {
    id: null,
    username: '',
    name: '',
    status: '',
  },
  friend: {
    id: null,
    telephone_number: null,
    username: '',
    name: '',
    avatar: '',
    metrics: {
      battery: '',
      local_time: '',
      location: {
        longitude: null,
        latitude: null,
        address: '',
      },
    },
    parsed: {
      parsedBattery: '',
      parsedTime: '',
    },
  },

  userInfoIsVisible: false,
  friendInfoIsReady: false,
  infoTextResponse: null,
};

export const friendInfoReducer = (state = defaultState, action) => {
  switch (action.type) {
    case SAVE_USER_INFO:
      const { payload: user } = action;
      var calculatedStatus = '';

      if (!user.verified) {
        if (user.allow_to_confirm) {
          calculatedStatus = FRIENDSHIP.AWAITING_CONFIRMATION_FULL;
        } else {
          calculatedStatus = FRIENDSHIP.REQUEST_SENT_FULL;
        }
      }
      if (user.verified === undefined) {
        calculatedStatus = FRIENDSHIP.NO_FRIENDSHIP_FULL;
      }

      return {
        ...state,
        user: {
          ...user,
          status: calculatedStatus,
        },
        userInfoIsVisible: true,
      };
    case FRIEND_INFO_SUCCESS:
      const { payload: friend } = action;

      moment.locale('ru');
      var parsedTime = moment(friend.metrics.local_time).startOf('hour').fromNow();

      var parsedBattery;
      if (friend.metrics.battery <= 75) {
        parsedBattery = 'battery-three-quarters';
        if (friend.metrics.battery <= 50) {
          parsedBattery = 'battery-half';
          if (friend.metrics.battery <= 25) {
            parsedBattery = 'battery-quarter';
          }
        }
      } else {
        parsedBattery = 'battery-full';
      }
      return {
        ...state,
        friend: {
          ...friend,
          parsed: {
            parsedBattery: parsedBattery,
            parsedTime: parsedTime,
          },
        },
        userInfoIsVisible: true,
        friendInfoIsReady: true,
      };
    case FRIEND_INFO_ERROR:
      return {
        ...state,
        infoTextResponse: action.payload,
      };
    case FRIEND_INFO_UPDATE_DATA:
      const { data: data } = action;
      return {
        ...state,
        friend: {
          ...state.friend,
          metrics: {
            battery: state.friend.metrics.battery,
            local_time: data.local_time,
            location: {
              longitude: data.longitude,
              latitude: data.latitude,
              address: data.address,
            },
          },
        },
      };
    case FRIEND_INFO_CLEAR:
      return {
        ...defaultState,
      };
  }

  return state;
};

export const SAVE_USER_INFO = 'SAVE_USER_INFO';
export const FRIEND_INFO_SUCCESS = 'FRIEND_INFO_SUCCESS';
export const FRIEND_INFO_ERROR = 'FRIEND_INFO_ERROR';
export const FRIEND_INFO_UPDATE_DATA = 'FRIEND_INFO_UPDATE_DATA';
export const FRIEND_INFO_CLEAR = 'FRIEND_INFO_CLEAR';

export const userInfoSaver = user => ({
  type: SAVE_USER_INFO,
  payload: user,
});

export const verifiedFriendInfoSuccess = friend => ({
  type: FRIEND_INFO_SUCCESS,
  payload: friend,
});

export const verifiedFriendInfoError = infoTextResponse => ({
  type: FRIEND_INFO_ERROR,
  payload: infoTextResponse,
});

export const updateFriendInfoData = data => ({
  type: FRIEND_INFO_UPDATE_DATA,
  data: data,
});

export const userInfoRemover = () => ({
  type: FRIEND_INFO_CLEAR,
});
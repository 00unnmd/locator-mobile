export const SEARCH_FRIENDS_LIST_VISIBILITY = 'SEARCH_FRIENDS_LIST_VISIBILITY';
export const SEARCH_FRIENDS_INPUT_VALUE = 'SEARCH_FRIENDS_INPUT_VALUE';
export const SEARCH_FRIENDS_SUCCESS = 'SEARCH_FRIENDS_SUCCESS';
export const SEARCH_FRIENDS_ERROR = 'SEARCH_FRIENDS_ERROR';
export const SEARCH_FRIENDS_DEFAULT_VALUES = 'SEARCH_FRIENDS_DEFAULT_VALUES';
export const SEARCH_FRIENDS_LOADING_STATUS = 'SEARCH_FRIENDS_LOADING_STATUS';

export const toggleSearchListVisibility = (visibility) => ({
  type: SEARCH_FRIENDS_LIST_VISIBILITY,
  payload: visibility,
});

export const saveInputValue = (value) => ({
  type: SEARCH_FRIENDS_INPUT_VALUE,
  payload: value,
});

export const searchFriendsSuccess = (data) => ({
  type: SEARCH_FRIENDS_SUCCESS,
  payload: data,
});

export const searchFriendsError = (searchTextResponse) => ({
  type: SEARCH_FRIENDS_ERROR,
  payload: searchTextResponse,
});

export const setDefaultValues = () => ({
  type: SEARCH_FRIENDS_DEFAULT_VALUES,
});

export const toggleLoadingStatus = (status) => ({
  type: SEARCH_FRIENDS_LOADING_STATUS,
  payload: status,
});

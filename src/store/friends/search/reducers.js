import {
  SEARCH_FRIENDS_LIST_VISIBILITY,
  SEARCH_FRIENDS_INPUT_VALUE,
  SEARCH_FRIENDS_SUCCESS,
  SEARCH_FRIENDS_ERROR,
  SEARCH_FRIENDS_DEFAULT_VALUES,
  SEARCH_FRIENDS_LOADING_STATUS,
} from './actions';

const defaultState = {
  searchListIsVisible: false,
  searchResultList: [],
  searchResultIsReady: false,
  searchInputValue: null,
  loadingProcessing: false,
  searchTextResponse: null,
};

export const searchFriendsReducer = (state = defaultState, action) => {
  switch (action.type) {
    case SEARCH_FRIENDS_LIST_VISIBILITY:
      return {
        ...state,
        searchListIsVisible: action.payload,
      };
    case SEARCH_FRIENDS_INPUT_VALUE:
      return {
        ...state,
        searchInputValue: action.payload,
      };
    case SEARCH_FRIENDS_SUCCESS:
      return {
        ...state,
        searchResultList: action.payload,
        searchResultIsReady: true,
        loadingProcessing: defaultState.loadingProcessing,
      };
    case SEARCH_FRIENDS_ERROR:
      return {
        ...state,
        searchTextResponse: action.payload,
        searchResultIsReady: true,
        loadingProcessing: defaultState.loadingProcessing,
      };
    case SEARCH_FRIENDS_DEFAULT_VALUES:
      return {
        ...state,
        searchResultList: defaultState.searchResultList,
        searchResultIsReady: defaultState.searchResultIsReady,
        searchInputValue: defaultState.searchInputValue,
        loadingProcessing: defaultState.loadingProcessing,
        searchTextResponse: defaultState.searchTextResponse,
      };
    case SEARCH_FRIENDS_LOADING_STATUS:
      return {
        ...state,
        loadingProcessing: action.payload,
      };
  }

  return state;
};

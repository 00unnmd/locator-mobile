import { ADD_FRIEND_CHANGE_TEXT_RESPONSE } from './actions';

const defaultState = {
  addFriendTextResponse: 'Добавить в друзья'
};

export const addFriendReducer = (state = defaultState, action) => {
  switch (action.type) {
    case ADD_FRIEND_CHANGE_TEXT_RESPONSE:
      return {
        ...state,
        addFriendTextResponse: action.payload
      };
  }

  return state;
};

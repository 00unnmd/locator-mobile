export const ADD_FRIEND_CHANGE_TEXT_RESPONSE = 'ADD_FRIEND_CHANGE_TEXT_RESPONSE';

export const changeButtonTitle = text => ({
  type: ADD_FRIEND_CHANGE_TEXT_RESPONSE,
  payload: text
});

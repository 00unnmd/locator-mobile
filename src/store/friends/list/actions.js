export const FRIENDS_LIST_SUCCESS = 'FRIENDS_LIST_SUCCESS';
export const FRIENDS_LIST_WRONG_SESSION_TOKEN = 'FRIENDS_LIST_WRONG_SESSION_TOKEN';
export const FRIENDS_LIST_UPDATE_FRIEND_LOCATION = 'FRIENDS_LIST_UPDATE_FRIEND_LOCATION';
export const FRIENDS_DRAWER_IS_VISIBLE = 'FRIENDS_DRAWER_IS_VISIBLE';

export const friendsListSuccess = list => ({
  type: FRIENDS_LIST_SUCCESS,
  payload: list,
});

export const friendsListWrongSessionToken = listTextResponse => ({
  type: FRIENDS_LIST_WRONG_SESSION_TOKEN,
  payload: listTextResponse,
});

export const updateFriendLocation = data => ({
  type: FRIENDS_LIST_UPDATE_FRIEND_LOCATION,
  data: data,
});

export const friendsDrawerToggler = friendsDrawerVisible => ({
  type: FRIENDS_DRAWER_IS_VISIBLE,
  payload: friendsDrawerVisible,
});

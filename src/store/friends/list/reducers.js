import {
  FRIENDS_LIST_SUCCESS,
  FRIENDS_LIST_WRONG_SESSION_TOKEN,
  FRIENDS_LIST_UPDATE_FRIEND_LOCATION,
  FRIENDS_DRAWER_IS_VISIBLE,
} from './actions';
import { FRIENDSHIP } from '../../../constants/constants';

const defaultState = {
  friendsList: [
    {
      friendItem: {
        id: null,
        username: '',
        name: '',
        avatar: '',
        verified: null,
        allow_to_confirm: null,
        location: {
          longitude: null,
          latitude: null,
        },
        status: null,
      },
    },
  ],

  friendsListIsReady: false,
  listTextResponse: null,

  friendsDrawerVisible: true,
};

export const friendsListReducer = (state = defaultState, action) => {
  switch (action.type) {
    case FRIENDS_LIST_SUCCESS:
      const { payload: list } = action;
      return {
        ...state,
        //по прилету с сервера списка друзей определяю, подтверждена ли дружба,
        //если нет, добавляю поле status, которое отображаю в FriendsItem
        friendsList: list.map(friendItem => {
          if (!friendItem.verified) {
            if (friendItem.allow_to_confirm) {
              return {
                ...friendItem,
                status: FRIENDSHIP.AWAITING_CONFIRMATION,
              };
            } else {
              return {
                ...friendItem,
                status: FRIENDSHIP.REQUEST_SENT,
              };
            }
          } else {
            return {
              ...friendItem,
            };
          }
        }),
        friendsListIsReady: true,
      };
    case FRIENDS_LIST_WRONG_SESSION_TOKEN:
      return {
        ...state,
        friendsListIsReady: true,
        listTextResponse: action.payload,
      };
    case FRIENDS_LIST_UPDATE_FRIEND_LOCATION:
      const { data: data } = action;
      return {
        ...state,
        //смена координат если пользователь
        //с подключенного кабеля найден в списке друзей
        friendsList: state.friendsList.map(friendItem => {
          if (friendItem.id === data.user_id) {
            return {
              ...friendItem,
              location: {
                longitude: data.longitude,
                latitude: data.latitude,
              },
            };
          } else {
            return {
              ...friendItem,
            };
          }
        }),
      };
    case FRIENDS_DRAWER_IS_VISIBLE:
      if (action.payload) {
        return {
          ...state,
          friendsDrawerVisible: true,
        };
      }
      else {
        return {
          ...defaultState,
          friendsDrawerVisible: false
        }
      }
  }

  return state;
};

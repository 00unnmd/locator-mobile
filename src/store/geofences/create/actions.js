export const TOGGLE_CREATE_GEOFENCE = 'TOGGLE_CREATE_GEOFENCE';
export const SAVE_CENTER_COORDINATES = 'SAVE_CENTER_COORDINATES';
export const CHANGE_GEOFENCE_RADIUS = 'CHANGE_GEOFENCE_RADIUS';
export const SAVE_GEOFENCE_RADIUS = 'SAVE_GEOFENCE_RADIUS';
export const CHANGE_GEOFENCE_NAME = 'CHANGE_GEOFENCE_NAME';
export const CHANGE_GEOFENCE_COLOR = 'CHANGE_GEOFENCE_COLOR';
export const SAVE_NEW_GEOFENCE = 'SAVE_NEW_GEOFENCE';
export const ERROR_NEW_GEOFENCE = 'ERROR_NEW_GEOFENCE';

export const createGeofenceToggler = visibility => ({
  type: TOGGLE_CREATE_GEOFENCE,
  payload: visibility,
});

export const centerCoordinatesSaver = center => ({
  type: SAVE_CENTER_COORDINATES,
  payload: center,
});

export const changeGeofenceRadius = radius => ({
  type: CHANGE_GEOFENCE_RADIUS,
  payload: radius,
});

export const geofenceRadiusSaver = () => ({
  type: SAVE_GEOFENCE_RADIUS,
});

export const changeGeofenceName = newName => ({
  type: CHANGE_GEOFENCE_NAME,
  payload: newName,
});

export const changeGeofenceColor = (hexColor, rgbaColor) => ({
  type: CHANGE_GEOFENCE_COLOR,
  hex: hexColor,
  rgba: rgbaColor
});

export const saveNewGeofence = () => ({
  type: SAVE_NEW_GEOFENCE,
});

export const errorNewGeofence = textResponse => ({
  type: ERROR_NEW_GEOFENCE,
  payload: textResponse,
});

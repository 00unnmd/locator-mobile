import {
  TOGGLE_CREATE_GEOFENCE,
  SAVE_CENTER_COORDINATES,
  CHANGE_GEOFENCE_RADIUS,
  SAVE_GEOFENCE_RADIUS,
  CHANGE_GEOFENCE_NAME,
  CHANGE_GEOFENCE_COLOR,
  SAVE_NEW_GEOFENCE,
  ERROR_NEW_GEOFENCE,
} from './actions';

const defaultState = {
  createGeofenceIsReady: false,

  geofenceCenterIsReady: false,
  geofenceRadiusIsReady: false,
  geofenceNameIsReady: false,

  newGeofence: {
    center: {
      longitude: null,
      latutude: null,
    },
    radius: 100,
    name: '',
    color: null,
  },
  rgbaColor: null,

  createGeofenceTextResponse: null,
};

export const createGeofenceReducer = (state = defaultState, action) => {
  switch (action.type) {
    case TOGGLE_CREATE_GEOFENCE: {
      if (action.payload === true) {
        return {
          ...state,
          createGeofenceIsReady: action.payload,
          geofenceCenterIsReady: true,
        };
      } else {
        return {
          ...defaultState,
        };
      }
    }
    case SAVE_CENTER_COORDINATES: {
      return {
        ...state,
        newGeofence: {
          ...state.newGeofence,
          center: {
            longitude: action.payload.longitude,
            latitude: action.payload.latitude,
          },
        },
        geofenceCenterIsReady: false,
        geofenceRadiusIsReady: true,
      };
    }
    case CHANGE_GEOFENCE_RADIUS: {
      return {
        ...state,
        newGeofence: {
          ...state.newGeofence,
          radius: action.payload,
        },
      };
    }
    case SAVE_GEOFENCE_RADIUS: {
      return {
        ...state,
        geofenceRadiusIsReady: false,
        geofenceNameIsReady: true,
      };
    }
    case CHANGE_GEOFENCE_NAME: {
      return {
        ...state,
        newGeofence: {
          ...state.newGeofence,
          name: action.payload,
        },
      };
    }
    case CHANGE_GEOFENCE_COLOR: {
      return {
        ...state,
        newGeofence: {
          ...state.newGeofence,
          color: action.hex,
        },
        rgbaColor: action.rgba,
      };
    }
    case SAVE_NEW_GEOFENCE: {
      return {
        ...defaultState,
      };
    }
    case ERROR_NEW_GEOFENCE: {
      return {
        ...state,
        createGeofenceTextResponse: action.payload,
      };
    }
  }

  return state;
};

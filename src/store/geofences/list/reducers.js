import {
  TOGGLE_GEOFENCE_LIST,
  SAVE_GEOFENCES_LIST,
  ERROR_GEOFENCES_LIST,
  TOGGLE_GEOFENCE_ITEM,
} from './actions';
import { GEOFENCES_LIST_STATUS } from '../../../constants/constants';

const defaultState = {
  geofencesListIsVisible: false,

  geofencesList: [],
  geofencesListIsReady: false,

  activeGeofenceCoordinates: null,

  textResponse: null,
};

export const geofencesListReducer = (state = defaultState, action) => {
  switch (action.type) {
    case TOGGLE_GEOFENCE_LIST: {
      if (action.payload) {
        return {
          ...state,
          geofencesListIsVisible: true,
        };
      } else {
        return {
          ...defaultState,
        };
      }
    }
    case SAVE_GEOFENCES_LIST: {
      if (action.payload.length > 0) {
        return {
          ...state,
          geofencesList: action.payload,
          geofencesListIsReady: true,
          textResponse: null,
        };
      }
      else {
        return {
          ...state,
          geofencesList: defaultState.geofencesList,
          textResponse: GEOFENCES_LIST_STATUS.EMPTY,
        }
      }
    }
    case ERROR_GEOFENCES_LIST: {
      return {
        ...state,
        textResponse: action.payload,
      };
    }
    case TOGGLE_GEOFENCE_ITEM: {
      return {
        ...state,
        activeGeofenceCoordinates: action.payload,
      };
    }
  }

  return state;
};

export const TOGGLE_GEOFENCE_LIST = 'TOGGLE_GEOFENCE_LIST';
export const SAVE_GEOFENCES_LIST = 'SAVE_GEOFENCES_LIST';
export const ERROR_GEOFENCES_LIST = 'ERROR_GEOFENCES_LIST';
export const TOGGLE_GEOFENCE_ITEM = 'TOGGLE_GEOFENCE_ITEM';

export const geofencesListToggler = isVisible => ({
  type: TOGGLE_GEOFENCE_LIST,
  payload: isVisible,
});

export const geofencesListSaver = geofencesList => ({
  type: SAVE_GEOFENCES_LIST,
  payload: geofencesList,
});

export const geofencesListError = textResponse => ({
  type: ERROR_GEOFENCES_LIST,
  payload: textResponse,
});

export const geofenceItemToggler = coordinates => ({
  type: TOGGLE_GEOFENCE_ITEM,
  payload: coordinates,
});

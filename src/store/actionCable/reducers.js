import { ACTION_CABLE_CREATE_CABLE, ACTION_CABLE_CREATE_CHANNEL } from './actions';

const defaultState = {
  actionCable: null,
  channel: null,
};

export const actionCableReducer = (state = defaultState, action) => {
  switch (action.type) {
    case ACTION_CABLE_CREATE_CABLE:
      return {
        ...state,
        actionCable: action.payload,
      };
    case ACTION_CABLE_CREATE_CHANNEL:
      return {
        ...state,
        channel: action.payload,
      };
  }

  return state;
};

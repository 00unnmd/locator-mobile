export const ACTION_CABLE_CREATE_CABLE = 'ACTION_CABLE_CREATE';
export const ACTION_CABLE_CREATE_CHANNEL = 'ACTION_CABLE_CREATE_CHANNEL';

export const createActionCable = (actionCable) => ({
  type: ACTION_CABLE_CREATE_CABLE,
  payload: actionCable,
});

export const createChannel = (channel) => ({
  type: ACTION_CABLE_CREATE_CHANNEL,
  payload: channel,
});

import { SAVE_MAP_REFERENCE } from './actions';

const defaultState = {
  mapRef: null,
};

export const mapRefReducer = (state = defaultState, action) => {
  switch (action.type) {
    case SAVE_MAP_REFERENCE: {
      return {
        ...state,
        mapRef: action.payload,
      };
    }
  }

  return state;
};

export const SAVE_MAP_REFERENCE = 'SAVE_MAP_REFERENCE';

export const mapRefSaver = mapRef => ({
  type: SAVE_MAP_REFERENCE,
  payload: mapRef,
});

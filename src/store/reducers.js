import { combineReducers } from 'redux';

import { USER_LOGOUT } from './actions';

//reducers
import { loginReducer } from './authentification/login/reducers';
import { signupReducer } from './authentification/signup/reducers';
import { loginControlReducer } from './authentification/login-control/reducers';
import { loginWrapReducer } from './authentification/login-wrap/reducers';
import { friendsListReducer } from './friends/list/reducers';
import { searchFriendsReducer } from './friends/search/reducers';
import { friendInfoReducer } from './friends/info/reducers';
import { addFriendReducer } from './friends/add/reducers';
import { actionCableReducer } from './actionCable/reducers';
import { geofencesListReducer } from './geofences/list/reducers';
import { createGeofenceReducer } from './geofences/create/reducers';
import { mapRefReducer } from './map/reducers';
import { createRouteReducer } from './route/create/reducers';
import { settingsReducer } from './settings/reducers';

const appReducer = combineReducers({
  login: loginReducer,
  signup: signupReducer,
  loginControl: loginControlReducer,
  loginWrap: loginWrapReducer,
  friendsList: friendsListReducer,
  searchFriends: searchFriendsReducer,
  friendInfo: friendInfoReducer,
  addFriend: addFriendReducer,
  actionCable: actionCableReducer,
  geofencesList: geofencesListReducer,
  createGeofence: createGeofenceReducer,
  mapRef: mapRefReducer,
  createRoute: createRouteReducer,
  settings: settingsReducer
});

const rootReducer = (state, action) => {
  if (action.type === USER_LOGOUT) {
    state = undefined;
  }

  return appReducer(state, action);
};

export default rootReducer;

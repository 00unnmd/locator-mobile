export const SAVE_ACCOUNT_SETTINGS = 'SAVE_ACCOUNT_SETTINGS';
export const ERROR_ACCOUNT_SETTINGS = 'ERROR_ACCOUNT_SETTINGS';
export const MODAL_TOGGLE = 'MODAL_TOGGLE';
export const DROP_DATA = 'DROP_DATA';

export const accountSettingsSaver = settings => ({
  type: SAVE_ACCOUNT_SETTINGS,
  payload: settings
});

export const accountSettingsError = textResponse => ({
  type: ERROR_ACCOUNT_SETTINGS,
  payload: textResponse
});

export const modalVisibilityToggler = (visibility) => ({
  type: MODAL_TOGGLE,
  payload: visibility
})

export const dropData = () => ({
  type: DROP_DATA
})
import { SAVE_ACCOUNT_SETTINGS, ERROR_ACCOUNT_SETTINGS, MODAL_TOGGLE, DROP_DATA } from './actions';

const defaultState = {
  settings: {

  },
  settingsTextResponse: null,
  settingsIsReady: false,

  modalVisibility: false,
};

export const settingsReducer = (state = defaultState, action) => {
  switch (action.type) {
    case SAVE_ACCOUNT_SETTINGS: {
      return {
        ...state,
        settings: action.payload,
        settingsIsReady: true
      }
    }
    case ERROR_ACCOUNT_SETTINGS: {
      return {
        ...state,
        settingsTextResponse: action.payload,
        settingsIsReady: true
      }
    }
    case MODAL_TOGGLE: {
      return {
        ...state,
        modalVisibility: action.payload
      }
    }
    case DROP_DATA: {
      return {
        ...defaultState
      }
    }
  }

  return state;
};
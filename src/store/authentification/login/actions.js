export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGIN_BAD_RESPONSE = 'LOGIN_BAD_RESPONSE';
export const LOGIN_CLEAR_DATA = 'LOGIN_CLEAR_DATA';

export const loginSuccess = (data) => ({
    type: LOGIN_SUCCESS,
    payload: { login: data.login, password: data.password }
});

export const loginError = (textResponse) => ({
    type: LOGIN_BAD_RESPONSE,
    payload: textResponse,
});

export const setDefault = () => ({
    type: LOGIN_CLEAR_DATA,
});
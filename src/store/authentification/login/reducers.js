import {     
    LOGIN_SUCCESS,
    LOGIN_BAD_RESPONSE,
    LOGIN_CLEAR_DATA,
} from './actions';

const defaultState = {
    user: {
        login: null,
        password: null,
    },

    incorrectData: false,
    textResponse: null,
};

export const loginReducer = (state = defaultState, action) => {
    switch(action.type) {
        case LOGIN_SUCCESS:
            return {
                ...state,
                user: action.payload
            }
        case LOGIN_BAD_RESPONSE:
            return {
                ...state,
                incorrectData: true,
                textResponse: action.payload,
            };
        case LOGIN_CLEAR_DATA:
            return {
                ...state,
                incorrectData: defaultState.incorrectData,
                textResponse: defaultState.textResponse,
            }
    };

    return state;
};
import { LOGIN_CONTROL_TOKEN_CONFIRMATION_BOOL } from './actions';

const defaultState = {
    tokenConfirmation: null,
};

export const loginControlReducer = (state = defaultState, action) => {
    switch(action.type) {
        case LOGIN_CONTROL_TOKEN_CONFIRMATION_BOOL:
            return {
                ...state,
                tokenConfirmation: action.payload,
            };
    };

    return state;
};
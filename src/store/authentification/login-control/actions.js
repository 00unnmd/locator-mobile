export const LOGIN_CONTROL_TOKEN_CONFIRMATION_BOOL = 'LOGIN_CONTROL_TOKEN_CONFIRMATION_BOOL';

export const setTokenConfirmation = (tokenConfirmation) => ({
    type: LOGIN_CONTROL_TOKEN_CONFIRMATION_BOOL,
    payload: tokenConfirmation,
});
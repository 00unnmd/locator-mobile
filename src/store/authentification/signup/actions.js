export const SIGN_UP_SUCCESS = 'SIGN_UP_SUCCESS';
export const SIGN_UP_BAD_RESPONSE = 'SIGN_UP_BAD_RESPONSE';
export const SIGN_UP_CLEAR_DATA = 'SIGN_UP_CLEAR_DATA';
export const SIGN_UP_BAD_PASSWORD_BOOL = 'SIGN_UP_BAD_PASSWORD_BOOL';
export const SIGN_UP_USERNAME_BUSY = 'SIGN_UP_USERNAME_BUSY';
export const SIGN_UP_PHONE_NUMBER_BUSY = 'SIGN_UP_PHONE_NUMBER_BUSY';

export const signupSuccess = (data) => ({
    type: SIGN_UP_SUCCESS,
    payload: { telephone_number: data.telephone_number, username: data.username,
               name: data.name, password: data.password }
});

export const signupError = (textResponse) => ({
    type: SIGN_UP_BAD_RESPONSE,
    payload: textResponse,
});

export const setDefault = () => ({
    type: SIGN_UP_CLEAR_DATA,
});

export const passwordsNotMatch = (textResponse) => ({
    type: SIGN_UP_BAD_PASSWORD_BOOL,
    payload: textResponse,
});

export const usernameBusy = (textResponse) => ({
    type: SIGN_UP_USERNAME_BUSY,
    payload: textResponse,
});

export const phoneNumberBusy = (textResponse) => ({
    type:SIGN_UP_PHONE_NUMBER_BUSY,
    payload: textResponse,
});
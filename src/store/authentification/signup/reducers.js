import {
    SIGN_UP_SUCCESS,
    SIGN_UP_BAD_RESPONSE,
    SIGN_UP_CLEAR_DATA,
    SIGN_UP_BAD_PASSWORD_BOOL,
    SIGN_UP_USERNAME_BUSY,
    SIGN_UP_PHONE_NUMBER_BUSY,
} from './actions'

const defaultState = {
    user: {
        telephone_number: '',
        username: '',
        name: '',
        password: '',
    },
    badNumber: false,
    badUsername: false,
    badPassword: false,
    incorrectData: false,
    textResponse: null,
}

export const signupReducer = (state = defaultState, action) => {
    switch(action.type) {
        case SIGN_UP_SUCCESS:
            return {
                ...state,
                user: action.payload
            };
        case SIGN_UP_BAD_RESPONSE:
            return {
                ...state,
                incorrectData: true,
                textResponse: action.payload,
            };
        case SIGN_UP_CLEAR_DATA:
            return {
                ...state,
                badNumber: defaultState.badNumber,
                badUsername: defaultState.badUsername,
                badPassword: defaultState.badPassword,
                incorrectData: defaultState.incorrectData,
                textResponse: defaultState.textResponse,
            };
        case SIGN_UP_BAD_PASSWORD_BOOL:
            return {
                ...state,
                badPassword: true,
                textResponse: action.payload,
            };
        case SIGN_UP_USERNAME_BUSY:
            return {
                ...state,
                badUsername: true,
                textResponse: action.payload,
            };
        case SIGN_UP_PHONE_NUMBER_BUSY:
            return {
                ...state,
                badNumber: true,
                textResponse: action.payload,
            };
    };

    return state;
};
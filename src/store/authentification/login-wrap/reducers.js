import { LOGIN_WRAP_LOGIN_STATUS_BOOL } from './actions';

const defaultState = {
    loginStatus: true,
};

export const loginWrapReducer = (state = defaultState, action) => {
    switch(action.type) {
        case LOGIN_WRAP_LOGIN_STATUS_BOOL:
            return {
                ...state,
                loginStatus: action.payload,
            };
    };

    return state;
};
export const LOGIN_WRAP_LOGIN_STATUS_BOOL = 'LOGIN_WRAP_LOGIN_STATUS_BOOL';

export const setLoginStatus = (loginStatus) => ({
    type: LOGIN_WRAP_LOGIN_STATUS_BOOL,
    payload: loginStatus,
});
export const TOGGLE_CREATE_ROUTE = 'TOGGLE_CREATE_ROUTE';
export const SAVE_NEW_ROUTE = 'SAVE_NEW_ROUTE';
export const ERROR_NEW_ROUTE = 'ERROR_NEW_ROUTE';
export const RESET_ROUTE = 'RESET_ROUTE';

export const createRouteToggler = (isVisible) => ({
  type: TOGGLE_CREATE_ROUTE,
  payload: isVisible
});

export const createRouteSaver = (coordinatesArray, strokeColorsArray) => ({
  type: SAVE_NEW_ROUTE,
  coordinatesArray: coordinatesArray,
  colorsArray: strokeColorsArray
});

export const createRouteError = textResponse => ({
  type: ERROR_NEW_ROUTE,
  payload: textResponse
});

export const resetRoute = () => ({
  type: RESET_ROUTE,
});
import {
  TOGGLE_CREATE_ROUTE,
  SAVE_NEW_ROUTE,
  ERROR_NEW_ROUTE,
  RESET_ROUTE
} from './actions';

const defaultState = {
  createRouteIsVisible: false,
  routeCoordinates: [],
  strokeColors: [],
  routeCoordinatesIsReady: false,
  textResponse: null,
};

export const createRouteReducer = (state = defaultState, action) => {
  switch (action.type) {
    case TOGGLE_CREATE_ROUTE: {
      if (action.payload) {
        return {
          ...state,
          createRouteIsVisible: true,
        }
      }
      else {
        return {
          ...defaultState,
        }
      }
    }
    case SAVE_NEW_ROUTE: {
      return {
        ...state,
        routeCoordinates: action.coordinatesArray,
        strokeColors: action.colorsArray,
        textResponse: defaultState.textResponse,
        routeCoordinatesIsReady: true,
      }
    }
    case ERROR_NEW_ROUTE: {
      return {
        ...state,
        routeCoordinates: defaultState.routeCoordinates,
        routeCoordinatesIsReady: defaultState.routeCoordinatesIsReady,
        textResponse: action.payload,
      }
    }
    case RESET_ROUTE: {
      return {
        ...defaultState,
        createRouteIsVisible: true
      }
    }
  };

  return state;
};
export const USER_LOGOUT = 'USER_LOGOUT';

export const clearStorage = () => ({
  type: USER_LOGOUT,
});

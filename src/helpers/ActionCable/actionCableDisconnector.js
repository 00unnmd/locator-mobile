import store from '../../store/store';

class actionCableDisconnector {
  constructor() {
    this.actionCableConsumer = store.getState().actionCable.actionCable.actionCableConsumer;
    this.actionCableConsumer.disconnect();
  }
}

const disconnectActionCable = () => {
  const actionCable = new actionCableDisconnector();

  return actionCable;
};

export default disconnectActionCable;

import { ActionCable, Cable } from '@kesha-antonov/react-native-action-cable';

import { getToken } from '../../api/axiosConfig/axiosConfig';
import { WEB_SOCKET_URL } from '../../constants/constants';

import store from '../../store/store';
import { createActionCable } from '../../store/actionCable/actions';

class actionCableConnector {
  constructor(token) {
    this.actionCableConsumer = ActionCable.createConsumer(`${WEB_SOCKET_URL}${token}`);
    this.cable = new Cable({});
  }
}

const connectActionCable = async () => {
  const token = await getToken();
  const actionCable = new actionCableConnector(token);

  store.dispatch(createActionCable(actionCable));
};

export default connectActionCable;

import store from '../../store/store';
import { createChannel } from '../../store/actionCable/actions';

import { updateFriendInfoData } from '../../store/friends/info/actions';
import { updateFriendLocation } from '../../store/friends/list/actions';

class channelSubscriber {
  constructor(friendId) {
    const actionCableConsumer = store.getState().actionCable.actionCable.actionCableConsumer;
    const cable = store.getState().actionCable.actionCable.cable;

    this.channel = cable.setChannel(
      'LocationsChannel',
      actionCableConsumer.subscriptions.create({
        channel: 'LocationsChannel',
        id: friendId,
      })
    );
    this.channel
      .on('received', this.handleReceived)
      .on('connected', this.handleConnected)
      .on('rejected', this.handleRejected)
      .on('disconnected', this.handleDisconnected);
  }

  handleReceived = (data) => {
    console.log('received data: ', data);

    store.dispatch(updateFriendInfoData(data));
    store.dispatch(updateFriendLocation(data));
  };
  handleConnected = () => {
    console.log('connected to webSocket');
  };
  handleRejected = () => {
    console.log('rejected from webSocket');
  };
  handleDisconnected = () => {
    console.log('disconnected from websocket');
  };
}

const subscribeChannel = (friendId) => {
  const channel = new channelSubscriber(friendId);

  store.dispatch(createChannel(channel));
};

export default subscribeChannel;

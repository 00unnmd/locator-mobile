import store from '../../store/store';

class channelUnsubscriber {
  constructor() {
    this.actionCable = store.getState().actionCable.actionCable;
    this.channel = store.getState().actionCable.channel;
    const channelName = 'LocationsChannel';

    this.channel.channel
      .removeListener('received', this.channel.handleReceived)
      .removeListener('connected', this.channel.handleConnected)
      .removeListener('rejected', this.channel.handleDisconnected)
      .removeListener('disconnected', this.channel.handleDisconnected);
    this.channel.channel.unsubscribe();
    delete this.actionCable.cable.channels[channelName];
    console.log('unsibscribed');
  }
}

const unsubscribeChannel = () => {
  const unsubscribe = new channelUnsubscriber();

  return unsubscribe;
};

export default unsubscribeChannel;

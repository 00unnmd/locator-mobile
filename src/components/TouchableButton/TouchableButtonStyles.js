import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  button: {
    alignItems: 'center',
    backgroundColor: '#fff',
    borderRadius: 10,
    flexDirection: 'row',
    justifyContent: 'center',
    padding: 10,
    minWidth: '49%',
  },
  buttonText: {
    textAlign: 'center',
    color: '#fff',
    fontSize: 15,
  },
  disabled: {
    backgroundColor: '#aaa',
  },
  largeSize: {
    minHeight: 70,
  },
  mediumSize: {
    minHeight: 50,
  },
  smallSize: {
    minHeight: 30,
  },
  largeFontSize: {
    fontSize: 18,
  },
  mediumFontSize: {
    fontSize: 16,
  },
  smallFontSize: {
    fontSize: 15,
  },
  accept: {
    backgroundColor: '#30c859',
  },
  decline: {
    backgroundColor: '#ff0530',
  },
  neutral: {
    backgroundColor: '#67addb',
  },
});
export default styles;

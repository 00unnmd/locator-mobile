import React from 'react';
import { View, Text } from 'react-native';
import { TouchableHighlight } from 'react-native-gesture-handler';
import { Icon } from 'react-native-elements';

import styles from './TouchableButtonStyles';

const TouchableButton = props => {
  return (
    <TouchableHighlight
      underlayColor='#111'
      style={{
        borderRadius: 10,
      }}
      onPress={() => {
        props.btnAction(props.data && props.data);
      }}
      disabled={props.disabled}
    >
      <View
        style={[
          styles.button,
          props.btnBgColor && props.btnBgColor == 'green'
            ? styles.accept
            : props.btnBgColor == 'red'
            ? styles.decline
            : styles.neutral,
          props.disabled && styles.disabled,
          props.btnSize == 'large'
            ? styles.largeSize
            : props.btnSize == 'medium'
            ? styles.mediumSize
            : styles.smallSize,
        ]}
      >
        {props.iconType && (
          <Icon
            type={props.iconType}
            name={props.iconName}
            size={props.iconSize == 'large' ? 18 : props.iconSize == 'medium' ? 16 : 15}
            color='#fff'
          />
        )}
        <Text
          style={[
            styles.buttonText,
            props.iconType && { paddingLeft: 5 },
            props.btnSize == 'large'
              ? styles.largeFontSize
              : props.btnSize == 'medium'
              ? styles.mediumFontSize
              : styles.smallFontSize,
          ]}
        >
          {props.btnTitle && props.btnTitle}
        </Text>
      </View>
    </TouchableHighlight>
  );
};

export default TouchableButton;

import React from 'react';
import { View, Text } from 'react-native';
import { Icon } from 'react-native-elements';
import { Feather } from '@expo/vector-icons';
import styles from './DrawerTitleStyles';

const DrawerTitle = props => {
  return (
    <View style={[styles.container, props.style]}>
      <Feather name='minus' size={30} style={styles.minusIcon} />
      <View style={styles.titleWithIcon}>
        <View style={styles.titleContainer}>
          <Text style={styles.titleText}>{`${props.title} `}</Text>
          {props.titleIcon && (
            <Icon type={props.titleIcon.type} name={props.titleIcon.name} size={15} />
          )}
        </View>
        <Icon
          type='ionicon'
          name={props.icon}
          size={23}
          iconStyle={styles.searchIcon}
          onPress={() => {
            props.iconAction();
          }}
        />
      </View>

      {props.subTitle && <Text style={styles.subTitleText}>{props.subTitle}</Text>}
    </View>
  );
};

export default DrawerTitle;

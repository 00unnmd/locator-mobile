import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    position: 'relative',
    marginBottom: 10,
    marginHorizontal: 10,
  },
  titleWithIcon: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  titleContainer: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
  },
  titleText: {
    fontSize: 23,
    fontWeight: 'bold',
    display: 'flex',
  },
  subTitleText: {
    fontSize: 12,
    marginTop: 5,
  },
  minusIcon: {
    position: 'absolute',
    alignSelf: 'center',
    top: -25,
    color: '#aaa',
  },
  searchIcon: {
    color: '#aaa',
  },
});

export default styles;

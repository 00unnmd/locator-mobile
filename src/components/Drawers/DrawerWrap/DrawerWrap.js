import React from 'react';
import { View } from 'react-native';
import BottomDrawer from 'rn-bottom-drawer';

import DrawerTitle from '../DrawerTitle/DrawerTitle';
import styles from './DrawerWrapStyles';

const DrawerWrap = props => {
  return (
    <BottomDrawer
      containerHeight={props.containerHeight}
      downDisplay={props.downDisplay}
      startUp={props.startUp}
      backgroundColor={'#f5f5f5'}
    >
      <View style={[styles.container, props.stylesContainer]}>
        <DrawerTitle {...props} />
        {props.content}
      </View>
    </BottomDrawer>
  );
};

export default DrawerWrap;

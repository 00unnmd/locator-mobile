import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    position: 'relative',
    marginTop: 20,
    marginHorizontal: 10,
  },
});

export default styles;

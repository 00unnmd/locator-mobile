import React, { useEffect, useRef, useState } from 'react';
import MapView, { Marker, Circle, Polyline, PROVIDER_GOOGLE } from 'react-native-maps';
import { Avatar, Icon } from 'react-native-elements';
import { View, Dimensions } from 'react-native';

import useFriendList from '../../hooks/friends/friendsList/useFriendsList';
import useSendMetrics from '../../hooks/metrics/sendMetrics/useSendMetrics';  //legacy
import useFriendInfo from '../../hooks/friends/friendInfo/useFriendInfo';
import useGeofencesList from '../../hooks/geofences/list/useGeofencesList';
import useCreateGeofence from '../../hooks/geofences/create/useCreateGeofence';
import useCreateRoute from '../../hooks/route/create/useCreateRoute';
import useMapRef from '../../hooks/map/useMapRef';

import Settings from '../Settings/Settings';

import styles from './MapStyles';

const windowHeight = Dimensions.get('window').height;

const Map = () => {
  const { friendsList, friendsListIsReady, friendsDrawerVisible } = useFriendList();
  const { friend, friendInfoIsReady } = useFriendInfo();
  const {
    geofencesList,
    activeGeofenceCoordinates,
    geofencesListIsVisible,
    geofencesListIsReady,
    toggleGeofenceItem,
  } = useGeofencesList();
  const {
    geofenceCenterIsReady,
    geofenceRadiusIsReady,
    geofenceNameIsReady,
    newGeofence,
    rgbaColor
  } = useCreateGeofence();
  const { routeCoordinates, strokeColors, routeCoordinatesIsReady } = useCreateRoute();
  const markerRef = useRef();
  const mapRef = useRef();
  const { saveMapRef } = useMapRef();

  //сохраняем экземпляр карты
  useEffect(() => {
    saveMapRef(mapRef.current);
  }, []);

  //анимация к иконке пользователя на карте
  useEffect(() => {
    if (friendInfoIsReady && friend.metrics && activeGeofenceCoordinates == null && !routeCoordinatesIsReady) {
      const region = {
        latitude: friend.metrics.location.latitude,
        longitude: friend.metrics.location.longitude,
        latitudeDelta: 0.004,
        longitudeDelta: 0.004,
      };
      const duration = 800;

      mapRef.current.animateToRegion(region, duration);
    }
  }, [friend, friendInfoIsReady, activeGeofenceCoordinates, routeCoordinatesIsReady]);
  //добавить возврат к пользователю по покиданию компонента GeofencesList

  //анимация к кругу геозоны
  useEffect(() => {
    if (activeGeofenceCoordinates != null) {
      mapRef.current.fitToCoordinates(activeGeofenceCoordinates, {
        edgePadding: {
          top: 20,
          right: 20,
          bottom: 20,
          left: 20
        },
        animated: true
      });
    }
  }, [activeGeofenceCoordinates]);

  const FriendListMarkers = () => {
    return (
      friendsListIsReady &&
      friendsList.map(
        user =>
          user.location && (
            <Marker
              key={user.id}
              coordinate={user.location}
              title={user.username}
              description={user.name}
            >
              <Avatar
                size='small'
                rounded
                source={{ uri: `${user.avatar}` }}
                title={user.name[0] + user.name[1]}
              />
            </Marker>
          )
      )
    );
  };
  const GeofencesListCircles = () => {
    const circleRefs = useRef({});

    return (
      geofencesListIsReady &&
      geofencesList.map(geofence => (
        <React.Fragment key={geofence.zone_id}>
          <Circle
            center={{
              latitude: geofence.center.latitude,
              longitude: geofence.center.longitude,
            }}
            radius={geofence.radius}
            ref={ref => {
              circleRefs.current[String(geofence.zone_id)] = ref;
            }}
            fillColor={geofence.color}
            onLayout={() => {
              circleRefs.current[String(geofence.zone_id)].setNativeProps({
                fillColor: `${geofence.color}`,
              });
            }}
          />
          <Marker
            coordinate={{
              latitude: geofence.center.latitude,
              longitude: geofence.center.longitude,
            }}
            title={geofence.name}
            description={`${geofence.radius}м`}
          />
        </React.Fragment>
      ))
    );
  };
  const NewGeofenceCircle = () => {
    const circleRef = useRef();

    useEffect(() => {
      if (markerRef.current) {
        markerRef.current.showCallout();
      }
    }, []);
    return (
      (geofenceRadiusIsReady || geofenceNameIsReady) && (
        <React.Fragment>
          <Circle
            center={{
              latitude: newGeofence.center.latitude,
              longitude: newGeofence.center.longitude,
            }}
            radius={newGeofence.radius}
            ref={circleRef}
            fillColor={rgbaColor && rgbaColor}
            onLayout={() => {
              newGeofence.color &&
                circleRef.current.setNativeProps({
                  fillColor: `${rgbaColor}`,
                });
            }}
          />
          <Marker
            coordinate={{
              latitude: newGeofence.center.latitude,
              longitude: newGeofence.center.longitude,
            }}
            title={newGeofence.name}
            description={`${newGeofence.radius}м`}
            ref={markerRef}
          />
        </React.Fragment>
      )
    );
  };
  const RoutePolyline = () => {
    const [markersActive, setMarkersActive] = useState(true);
    let friendCoordinates = friend.metrics.location;

    //animate to route region
    useEffect(() => {
      if (routeCoordinatesIsReady) {
        mapRef.current.fitToCoordinates(routeCoordinates, {
          edgePadding: {
            top: 20,
            right: 20,
            bottom: 20,
            left: 20
          },
          animated: true
        });
      }
    }, [routeCoordinatesIsReady]);

    return (
      routeCoordinatesIsReady && (
        <React.Fragment>
          <Polyline
            coordinates={routeCoordinates}
            strokeWidth={3}
            strokeColor='#67addb'
            tappable={true}
            onPress={() => { setMarkersActive(!markersActive) }}
          />
          {routeCoordinates.map((routeItem, i) => (
            <React.Fragment key={i} >
              <Circle
                center={routeItem}
                radius={5}
              />
              {markersActive && !(friendCoordinates.latitude === routeItem.latitude && friendCoordinates.longitude === routeItem.longitude) &&
                <Marker
                  coordinate={routeItem}
                  title={`№${i + 1} ${routeItem.timestamp.date}`}
                  description={routeItem.timestamp.time}
                  pinColor={strokeColors[i]}
                />
              }
            </React.Fragment>
          ))}
        </React.Fragment>
      ));
  };

  return (
    <View style={{ flex: 1 }}>
      <MapView
        provider={PROVIDER_GOOGLE}
        initialRegion={{
          latitude: 54.727956,
          longitude: 55.951486,
          latitudeDelta: 0.1,
          longitudeDelta: 0.1,
        }}
        mapPadding={{
          top: 0,
          right: 0,
          bottom: (windowHeight - 20) / 2,
          left: 0,
        }}
        showsUserLocation={true}
        showsMyLocationButton={true}
        showsCompass={true}
        zoomEnabled={(friend.metrics && !friendInfoIsReady) || geofencesListIsVisible || routeCoordinatesIsReady}
        rotateEnabled={(friend.metrics && !friendInfoIsReady) || geofencesListIsVisible || routeCoordinatesIsReady}
        scrollEnabled={(friend.metrics && !friendInfoIsReady) || geofencesListIsVisible || routeCoordinatesIsReady}
        loadingEnabled={true}
        ref={mapRef}
        style={styles.mapStyle}
      >
        <FriendListMarkers />
        <GeofencesListCircles />
        <NewGeofenceCircle />
        <RoutePolyline />
      </MapView>
      {geofenceCenterIsReady && (
        <View
          pointerEvents='none'
          style={{
            position: 'absolute',
            top: 0,
            bottom: '50%',
            left: 0,
            right: 0,
            alignItems: 'center',
            justifyContent: 'center',
            backgroundColor: 'transparent',
          }}
        >
          <Icon type='font-awesome' name='map-pin' size={30} />
        </View>
      )}
      {friendsDrawerVisible && (
        <View
          style={{
            position: 'absolute',
            top: 25,
            left: 10,
          }}
        >
          <Settings />
        </View>
      )}
    </View>
  );
};

export default Map;

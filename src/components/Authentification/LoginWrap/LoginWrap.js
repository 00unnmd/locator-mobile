import React from 'react';
import { useSelector } from 'react-redux';

//components
import SignUp from '../Signup/SignUp';
import LogIn from '../Login//LogIn';

const LoginWrap = () => {
  const { loginStatus } = useSelector((state) => ({
    loginStatus: state.loginWrap.loginStatus,
  }));

  return loginStatus ? <LogIn /> : <SignUp />;
};

export default LoginWrap;

import React, { useEffect } from 'react';
import * as SecureStore from 'expo-secure-store';
import { useSelector, useDispatch } from 'react-redux';

import { setTokenConfirmation } from '../../../store/authentification/login-control/actions';
import Loading from '../../Loading/Loading';
import LoginWrap from '../LoginWrap/LoginWrap';

import Map from '../../Map/Map';
import Friends from '../../Friends/Friends';
import connectActionCable from '../../../helpers/ActionCable/actionCableConnector';
import disconnectActionCable from '../../../helpers/ActionCable/actionCableDisconnector';
import useNotifications from '../../../hooks/permissions/notifications/useNotifications';
import useLocations from '../../../hooks/permissions/locations/useLocations';

const MainScreen = () => {
  const { registerForPushNotificationsAsync } = useNotifications();
  const { askForLocationPermissionsAsync } = useLocations();

  useEffect(() => {
    //открытие кабеля по входу в аккаунт
    connectActionCable();

    //отправляю токен устройства для получения уведомлений
    registerForPushNotificationsAsync();

    //спрашиваю передачу геолокации
    askForLocationPermissionsAsync();

    //закрытие кабеля по выходу из аккаунта
    return () => {
      disconnectActionCable();
    }
  }, []);

  return (
    <React.Fragment>
      <Map />
      <Friends />
    </React.Fragment>
  )
};

export const LoginControl = () => {
  const { tokenConfirmation } = useSelector((state) => ({
    tokenConfirmation: state.loginControl.tokenConfirmation,
  }));
  const dispatch = useDispatch();

  const fetchToken = async () => {
    return await SecureStore.getItemAsync('loginToken');
  };
  useEffect(() => {
    fetchToken().then((tokenStorage) => {
      //token is exist
      if (typeof tokenStorage === 'string') {
        dispatch(setTokenConfirmation(true));
      }
      //token doesn't exist
      if (tokenStorage == null) {
        dispatch(setTokenConfirmation(false));
      }
    });
  }, []);

  return tokenConfirmation ? <MainScreen /> : <LoginWrap />;
};

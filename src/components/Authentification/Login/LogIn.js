import React, { useState, useEffect } from 'react';
import { Text, View, TextInput, Button } from 'react-native';

import useLogin from '../../../hooks/authentification/login/useLogin';
import styles from './LogInStyles';

const LogIn = () => {
  const [login, setLogin] = useState();
  const [password, setPassword] = useState();

  const {
    incorrectData,
    textResponse,
    onStatusChange,
    clearFields,
    loginRequestProcessing,
  } = useLogin();

  useEffect(() => {
    return () => {
      clearFields();
    };
  }, []);

  const handleSubmit = () => {
    const data = { login, password };
    clearFields();
    loginRequestProcessing(data);
  };

  return (
    <View style={styles.container}>
      <Text style={styles.titleText}>Вход</Text>
      <TextInput
        style={[styles.input, incorrectData && styles.incorrectData]}
        textContentType='telephoneNumber'
        placeholder='Логин'
        keyboardType='numbers-and-punctuation'
        returnKeyType='next'
        maxLength={18}
        value={login}
        onChangeText={(text) => {
          setLogin(text);
        }}
      />
      <TextInput
        style={[styles.input, incorrectData && styles.incorrectData]}
        textContentType='password'
        placeholder='Пароль'
        returnKeyType='next'
        secureTextEntry={true}
        maxLength={20}
        value={password}
        onChangeText={(text) => {
          setPassword(text);
        }}
      />
      <Text style={styles.responseHint}>{textResponse}</Text>
      <Button
        title='Войти'
        onPress={() => {
          handleSubmit();
        }}
      />
      <View style={styles.bottomButton}>
        <Button
          title='Нет аккаунта? Зарегистрироваться'
          onPress={() => {
            onStatusChange(false);
          }}
        />
      </View>
    </View>
  );
};

export default LogIn;

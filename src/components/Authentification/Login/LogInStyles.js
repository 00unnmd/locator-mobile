import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#f5f5f5',
        position: 'relative',
    },
    input: {
        backgroundColor: '#eee',
        width: '90%',
        height: 40,
        borderRadius: 5,
        padding: 10,
        margin: 5,
        borderWidth: 1,
        borderColor: '#eee'
    },
    titleText: {
        fontSize: 20,
        fontWeight: 'bold',
        marginBottom: 10,
    },
    bottomButton: {
        position: 'absolute',
        bottom: 0,
    },
    incorrectData: {
        borderColor: '#ff063a',
        borderWidth: 1,
    },
    responseHint: {
        color: '#ff063a',
        height: 20,
    },
})

export default styles;
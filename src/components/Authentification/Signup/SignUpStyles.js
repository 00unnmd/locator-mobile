import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#f5f5f5'
    },
    input: {
        backgroundColor: '#eee',
        width: '90%',
        height: 40,
        borderRadius: 5,
        padding: 10,
        marginVertical: 5,
        borderWidth: 1,
        borderColor: '#eee'
    },
    titleText: {
        fontSize: 20,
        fontWeight: 'bold',
        marginBottom: 10,
    },
    absoluteBack: {
        position: 'absolute',
        top: '3%',
        left: '3%',
        flexDirection: 'row',
        alignItems: 'center',
    },
    blueColor: {
        color: '#007AFF',
    },
    incorrectData: {
        borderColor: '#ff063a',
        borderWidth: 1,
    },
    responseHint: {
        color: '#ff063a',
        height: 20,
    },
    placeholderStyle: {
        backgroundColor: '#eee',
    },
    avatarWrap: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        width: '90%'
    },
    avatarBlock: {
        width: '25%',
        marginRight: '5%'
    },
    rowNumberAndUsername: {
        width: '70%',
    },
    avatarInput: {
        width: '100%',
    }
})

export default styles;
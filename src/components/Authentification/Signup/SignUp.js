import React, { useState, useEffect } from 'react';
import { Text, View, TextInput, Button } from 'react-native';
import { Ionicons } from 'react-native-vector-icons';
import { TextInputMask } from 'react-native-masked-text';
import * as ImagePicker from 'expo-image-picker';
import { Avatar } from 'react-native-elements';

import useSignup from '../../../hooks/authentification/signup/useSignup';
import { BASE64 } from '../../../api/axiosConfig/axiosConfig';
import styles from './SignUpStyles';

const SignUp = () => {
    const [phoneNumber, setPhoneNumber] = useState();
    const [telephone_number, setTelephone_number] = useState();
    const [username, setUsername] = useState();
    const [name, setName] = useState();
    const [password, setPassword] = useState();
    const [confirmPassword, setConfirmPassword] = useState();
    const [avatar, setAvatar] = useState();

    const { badNumber, badUsername, badPassword, incorrectData, textResponse,
            clearFields, onStatusChange, signupRequestProcessing } = useSignup();

    useEffect(() => {
        return () => {
            clearFields();
        }
    }, []);

    const handleSubmit = () => {
        const data = { telephone_number, username, name, avatar, password };
        clearFields();
        signupRequestProcessing(data, confirmPassword)
    };

    const avatarPicker = async() => {
        const image = await ImagePicker.launchImageLibraryAsync({
            allowsEditing: true,
            aspect: [150, 150],
            base64: true
        });
        setAvatar(BASE64 + image.base64)
    }

    return(
        <View style={styles.container}>
            <View style={styles.absoluteBack}>
                <Ionicons
                    name={'ios-arrow-back'}
                    size={20}
                    style={styles.blueColor}
                />
                <Button 
                    title='Назад'
                    onPress={() => {onStatusChange(true)}}
                />
            </View>
            <Text style={styles.titleText}>Регистрация</Text>
            <View style={styles.avatarWrap}>
                <View style={styles.avatarBlock}>
                    <Avatar 
                        size='large'
                        rounded
                        activeOpacity={0.7}
                        icon={{name: 'user', type: 'font-awesome', color: '#ccc'}}                
                        placeholderStyle={styles.placeholderStyle}
                        source={{uri: avatar}}
                        onPress={() => {avatarPicker()}}
                    />
                </View>
                <View style={styles.rowNumberAndUsername}>
                    <TextInputMask
                        style={[
                            styles.input,
                            styles.avatarInput,
                            badNumber && styles.incorrectData,
                            incorrectData && styles.incorrectData,
                        ]}
                        placeholder='Номер телефона'
                        textContentType='telephoneNumber'
                        keyboardType='numbers-and-punctuation'
                        maxLength={18}
                        returnKeyType='next'
                        value={phoneNumber}
                        type={'custom'}
                        options={{
                            mask: '+7 (999) 999-99-99',
                            getRawValue: (value) => {
                                return value.replace(/[+ ( ) -]/g, '');
                            },
                        }}
                        includeRawValueInChangeText={true}
                        onChangeText={(masked, raw) => {
                            setPhoneNumber(masked);
                            setTelephone_number(raw)
                        }}
                    />
                    <TextInput
                        style={[
                            styles.input,
                            styles.avatarInput,
                            badUsername && styles.incorrectData,
                            incorrectData && styles.incorrectData
                        ]}
                        textContentType='username'
                        placeholder='Имя пользователя'
                        returnKeyType='next'
                        maxLength={18}
                        value={username}
                        onChangeText={(text) => {setUsername(text)}}
                    />
                </View>
            </View>
            <TextInput
                style={[
                    styles.input,
                    incorrectData && styles.incorrectData
                ]} 
                textContentType='name'
                placeholder='Имя'
                returnKeyType='next'
                maxLength={10}
                value={name}
                onChangeText={(text) => {setName(text)}}
            />
            <TextInput
                style={[
                    styles.input,
                    badPassword && styles.incorrectData,
                    incorrectData && styles.incorrectData,
                ]} 
                textContentType='password'
                placeholder='Пароль'
                returnKeyType='next'
                secureTextEntry={true}
                maxLenght={20}
                value={password}
                onChangeText={(text) => {setPassword(text)}}
            />
            <TextInput
                style={[
                    styles.input,
                    badPassword && styles.incorrectData,
                    incorrectData && styles.incorrectData,
                ]} 
                textContentType='password'
                placeholder='Подтверждение пароля'
                returnKeyType='next'
                secureTextEntry={true}
                maxLength={20}
                value={confirmPassword}
                onChangeText={(text) => {setConfirmPassword(text)}}
            />
            <Text 
                style={styles.responseHint}
            >
                {textResponse}
            </Text>
            <Button
                title='Зарегистрироваться'
                onPress={() => {handleSubmit()}}
            />
        </View>
    );
};

export default SignUp;
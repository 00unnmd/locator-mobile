import React, { useEffect } from 'react';

import useSearchFriends from '../../../hooks/friends/searchFriends/useSearchFriends';
import useFriendInfo from '../../../hooks/friends/friendInfo/useFriendInfo';

import SearchField from '../SearchField/SearchField';
import FriendsItem from '../../Friends/FriendsItem/FriendsItem';
import Loading from '../../Loading/Loading';
import TextResponse from '../../TextResponse/TextResponse';

const SearchList = () => {
  const {
    searchResultList,
    searchResultIsReady,
    loadingProcessing,
    searchTextResponse,
    searchInputValue,
    searchFriendsRequestProcessing,
  } = useSearchFriends();
  const { showUserInfo } = useFriendInfo();

  const handleOnBlur = async search_name => {
    await searchFriendsRequestProcessing(search_name);
  };

  return (
    <React.Fragment>
      <SearchField
        containerStyle={{ paddingTop: 0 }}
        onBlur={handleOnBlur}
        textInputStore={searchInputValue}
      />

      {loadingProcessing ? (
        <Loading />
      ) : searchTextResponse ? (
        <TextResponse text={searchTextResponse} />
      ) : searchResultIsReady ? (
        <FriendsItem array={searchResultList} onItemClick={showUserInfo} />
      ) : (
        <TextResponse text='Найдите кого-нибудь!' />
      )}
    </React.Fragment>
  );
};

export default SearchList;

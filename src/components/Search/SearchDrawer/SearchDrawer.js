import React from 'react';
import { Dimensions } from 'react-native';

import useFriendsList from '../../../hooks/friends/friendsList/useFriendsList';
import useSearchFriends from '../../../hooks/friends/searchFriends/useSearchFriends';

import DrawerWrap from '../../Drawers/DrawerWrap/DrawerWrap';
import SearchList from '../SearchList/SearchList';

const windowHeight = Dimensions.get('window').height; //need for calculate height of drawer & downDisplay

const SearchDrawer = () => {
  const { showFriendListDrawer } = useFriendsList();
  const { toggleSearchList, setDefault } = useSearchFriends();

  const closeSearchDrawer = () => {
    toggleSearchList(false);
    setDefault();
    showFriendListDrawer();
  };

  return (
    <DrawerWrap
      containerHeight={windowHeight - 20}
      downDisplay={1}
      startUp={true}
      icon='ios-close-circle'
      title='Найти друзей'
      iconAction={closeSearchDrawer}
      stylesContainer={{ marginHorizontal: 0 }}
      content={<SearchList />}
    />
  );
};

export default SearchDrawer;

import React, { useState, useEffect } from 'react';
import { SearchBar } from 'react-native-elements';

const SearchField = props => {
  const [search_name, setSearch_name] = useState();

  useEffect(() => {
    setSearch_name(props.textInputStore);
  }, []);

  return (
    <SearchBar
      placeholder='Имя пользователя...'
      onChangeText={text => {
        setSearch_name(text);
      }}
      value={search_name}
      platform='ios'
      cancelButtonTitle=''
      clearIcon={null}
      containerStyle={props.containerStyle}
      inputContainerStyle={{ marginLeft: 10, marginRight: 10 }}
      cancelButtonProps={{
        buttonTextStyle: {
          paddingHorizontal: 4,
        },
      }}
      onBlur={() => {
        props.onBlur(search_name);
      }}
      autoFocus={true}

      //try on submit
    />
  );
};

export default SearchField;

import React from 'react';
import { View, Text, ActivityIndicator } from 'react-native';
import styles from './LoadingStyles';

const Loading = () => {
    return(
        <View style={styles.container}>
            <ActivityIndicator />
            <Text>Загружаем данные...</Text>
        </View>
    );
}

export default Loading;
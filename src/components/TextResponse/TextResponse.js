import React from 'react';
import { Text } from 'react-native';
import styles from './TextResponseStyles';

const TextResponse = props => {
  return <Text style={[styles.textPosition]}>{props.text}</Text>;
};

export default TextResponse;

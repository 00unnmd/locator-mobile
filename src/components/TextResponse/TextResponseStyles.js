import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  textPosition: {
    textAlign: 'center'
  }
});

export default styles;

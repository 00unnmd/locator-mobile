import React from 'react';
import { View, Text } from 'react-native';
import { Icon } from 'react-native-elements';

import styles from './MetricsItemStyles';

//Иконка с полем из объекта метрик
const MetricsItem = props => {
  return (
    <View style={styles.metricsInfo}>
      {props.reversed ? (
        <React.Fragment>
          <Text style={styles.addressText}>{props.textContent}</Text>
          <Icon type='font-awesome' name={props.iconName} size={14} color='#aaa' />
        </React.Fragment>
      ) : (
        <React.Fragment>
          <Icon type='font-awesome' name={props.iconName} size={14} color='#aaa' />
          <Text style={styles.addressText}>{props.textContent}</Text>
        </React.Fragment>
      )}
    </View>
  );
};

export default MetricsItem;

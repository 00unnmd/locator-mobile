import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
  },
  title: {
    fontSize: 17,
    fontWeight: 'bold',
  },
  smallTitle: {
    fontSize: 17,
    fontWeight: 'bold',
    alignSelf: 'center',
  },
  twoButtonContainer: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 10,
  },
  spaceBetweenContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  greyLabel: {
    fontSize: 10,
    color: '#aaa',
  },
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  modalView: {
    backgroundColor: 'white',
    borderRadius: 20,
    padding: 15,
    minWidth: '90%',
    minHeight: '80%',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    position: 'relative',
  },
  modalTitle: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  inputStyle: {
    marginVertical: 10,
    paddingHorizontal: 5,
    paddingVertical: 5,
    minHeight: 36,
    borderWidth: 1,
    borderColor: '#dcdce1',
    borderRadius: 9,
    backgroundColor: '#dcdce1',
    fontSize: 18,
  },
});

export default styles;

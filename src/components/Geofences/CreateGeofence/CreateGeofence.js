import React, { useEffect, useState } from 'react';
import { View, Text, Modal, TouchableOpacity, TextInput, Alert } from 'react-native';
import Slider from 'react-native-slider';
import { Icon } from 'react-native-elements';
import { ColorPicker, fromHsv } from 'react-native-color-picker';

import useCreateGeofence from '../../../hooks/geofences/create/useCreateGeofence';
import useFriendInfo from '../../../hooks/friends/friendInfo/useFriendInfo';
import TouchableButton from '../../TouchableButton/TouchableButton';

import styles from './CreateGeofenceStyles';

const CreateGeofence = () => {
  const {
    geofenceCenterIsReady,
    geofenceRadiusIsReady,
    geofenceNameIsReady,
    saveCenterCoordinates,
    saveGeofenceRadius,
    closeCreateGeofence,
    updateGeofenceRadius,
    updateGeofenceName,
    updateGeofenceColor,
    newGeofence,
    createNewGeofence,
    createGeofenceTextResponse,
  } = useCreateGeofence();
  const { friend } = useFriendInfo();

  useEffect(() => {
    return () => {
      closeCreateGeofence();
    };
  }, []);
  useEffect(() => {
    if (createGeofenceTextResponse != null) {
      createTwoButtonAlert();
    }
  }, [createGeofenceTextResponse]);

  const CenterPicker = () => {
    return (
      <View style={styles.container}>
        <Text style={styles.title}>1.Выберите центр геозоны</Text>
        <View style={styles.twoButtonContainer}>
          <TouchableButton
            btnSize='medium'
            btnAction={saveCenterCoordinates}
            btnTitle='Далее'
            btnBgColor='green'
            iconType='font-awesome'
            iconName='forward'
          />
          <TouchableButton
            btnSize='medium'
            btnAction={closeCreateGeofence}
            btnTitle='Отмена'
            btnBgColor='red'
            iconType='font-awesome'
            iconName='ban'
          />
        </View>
      </View>
    );
  };

  const RadiusPicker = () => {
    return (
      <View style={styles.container}>
        <View style={styles.spaceBetweenContainer}>
          <Text style={styles.title}>2.Укажите радиус</Text>
          <Text style={{ color: '#aaa' }}>{`${newGeofence.radius}м`}</Text>
        </View>
        <View style={[styles.spaceBetweenContainer, { marginBottom: -5, marginTop: 10 }]}>
          <Text style={styles.greyLabel}>100м</Text>
          <Text style={styles.greyLabel}>1000м</Text>
        </View>
        <Slider
          animateTransitions={true}
          minimumValue={100}
          maximumValue={1000}
          step={100}
          thumbTintColor='#aaa'
          minimumTrackTintColor='#FFFFFF'
          maximumTrackTintColor='#FFFFFF'
          value={newGeofence.radius}
          onSlidingComplete={radiusValue => {
            updateGeofenceRadius(radiusValue);
          }}
        />
        <View style={styles.twoButtonContainer}>
          <TouchableButton
            btnSize='medium'
            btnAction={saveGeofenceRadius}
            btnTitle='Далее'
            btnBgColor='green'
            iconType='font-awesome'
            iconName='forward'
          />
          <TouchableButton
            btnSize='medium'
            btnAction={closeCreateGeofence}
            btnTitle='Отмена'
            btnBgColor='red'
            iconType='font-awesome'
            iconName='ban'
          />
        </View>
      </View>
    );
  };

  const NameAndColorPicker = () => {
    const [colorModalVisibility, setColorModalVisibility] = useState(false);

    const InfoPickerModal = () => {
      const [localColor, setLocalColor] = useState({ h: 81, s: 37, v: 100 });
      const [localName, setLocalName] = useState('');
      const [localError, setLocalError] = useState(false);

      const handleApllyInfo = (geofenceName, geofenceColor) => {
        setLocalError(false);
        if (geofenceName.length < 1) {
          setLocalError(true);
        } else {
          updateGeofenceName(geofenceName);
          updateGeofenceColor(fromHsv(geofenceColor));
          setColorModalVisibility(!colorModalVisibility);
        }
      };

      return (
        <Modal animationType='slide' transparent={true} visible={colorModalVisibility}>
          <View style={styles.centeredView}>
            <View style={styles.modalView}>
              <View style={styles.modalTitle}>
                <Text style={styles.smallTitle}>Название и цвет геозоны</Text>
                <TouchableOpacity
                  onPress={() => {
                    setColorModalVisibility(!colorModalVisibility);
                  }}
                >
                  <Icon type='ionicon' name='ios-close-circle' size={23} color='#aaa' />
                </TouchableOpacity>
              </View>

              <TextInput
                placeholder='Название новой геозоны...'
                style={styles.inputStyle}
                value={localName}
                maxLength={20}
                onChangeText={newName => {
                  setLocalName(newName);
                }}
              />
              <ColorPicker
                color={localColor}
                onColorChange={newColor => {
                  setLocalColor(newColor);
                }}
                defaultColor='#aaa'
                style={{ flex: 1 }}
              />
              <TouchableButton
                btnSize='small'
                btnAction={() => {
                  handleApllyInfo(localName, localColor);
                }}
                btnTitle={localError ? 'Введены не все данные!' : 'Применить'}
                btnBgColor={localError ? 'red' : 'green'}
                iconType='font-awesome'
                iconName={localError ? 'ban' : 'check'}
              />
            </View>
          </View>
        </Modal>
      );
    };

    return (
      <View style={styles.container}>
        <Text style={styles.title}>3.Введите название и выберите цвет.</Text>
        <View style={{ marginTop: 10 }}>
          <TouchableButton
            btnSize='medium'
            btnAction={() => {
              setColorModalVisibility(!colorModalVisibility);
            }}
            btnTitle='Название и цвет'
            btnBgColor='neutral'
            iconType='font-awesome'
            iconName='cog'
          />
        </View>
        <View style={styles.twoButtonContainer}>
          <TouchableButton
            btnSize='medium'
            btnAction={() => {
              createNewGeofence(friend, newGeofence);
            }}
            btnTitle='Создать'
            btnBgColor='green'
            iconType='font-awesome'
            iconName='plus'
            disabled={!Boolean(newGeofence.name)}
          />
          <TouchableButton
            btnSize='medium'
            btnAction={closeCreateGeofence}
            btnTitle='Отмена'
            btnBgColor='red'
            iconType='font-awesome'
            iconName='ban'
          />
        </View>
        <InfoPickerModal />
      </View>
    );
  };

  const createTwoButtonAlert = () =>
    Alert.alert(
      'Геозона не создана.',
      `${createGeofenceTextResponse}`,
      [
        {
          text: 'Закрыть',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
      ],
      { cancelable: false }
    );

  return geofenceCenterIsReady ? (
    <CenterPicker />
  ) : geofenceRadiusIsReady ? (
    <RadiusPicker />
  ) : (
    geofenceNameIsReady && <NameAndColorPicker />
  );
};

export default CreateGeofence;

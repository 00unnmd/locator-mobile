import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    marginHorizontal: -10,
    flex: 1,
  },
  listItemContainer: {
    paddingHorizontal: 10,
    paddingVertical: 10,
    backgroundColor: '#f5f5f5',
    borderTopWidth: 0.5,
    borderBottomWidth: 0.5,
    borderTopColor: '#eee',
    borderBottomColor: '#eee',
  },
  titleStyle: {
    fontSize: 15,
  },
  subtitleStyles: {
    fontSize: 12,
  },
  placeholderStyle: {
    backgroundColor: '#aaa',
  },
});

export default styles;

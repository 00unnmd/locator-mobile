import React, { useEffect } from 'react';
import { ScrollView, View, Alert } from 'react-native';
import { ListItem, Icon } from 'react-native-elements';

import useGeofencesList from '../../../hooks/geofences/list/useGeofencesList';
import useCreateGeofence from '../../../hooks/geofences/create/useCreateGeofence';
import useDeleteGeofence from '../../../hooks/geofences/delete/useDeleteGeofence';

import CreateGeofence from '../CreateGeofence/CreateGeofence';
import TouchableButton from '../../TouchableButton/TouchableButton';
import Loading from '../../Loading/Loading';
import TextResponse from '../../TextResponse/TextResponse';

import styles from './GeofencesListStyles';

const GeofencesList = props => {
  const {
    geofencesList,
    geofencesListIsReady,
    textResponse,
    geofencesListRequestProcessing,
    closeGeofenceList,
    toggleGeofenceItem,
  } = useGeofencesList();
  const { createGeofenceIsReady, showCreateGeofence } = useCreateGeofence();
  const { deleteGeofenceRequestProcessing } = useDeleteGeofence();

  useEffect(() => {
    geofencesListRequestProcessing(props.friend);
  }, []);

  const deleteGeofence = (friend, geofence) => {
    Alert.alert(`Вы действительно хотите удалить геозону ${geofence.name}(${geofence.radius}м)?`, '', [
      {
        text: 'Отмена',
        style: 'cancel',
      },
      {
        text: 'Удалить',
        style: 'destructive',
        onPress: async() => {
          let res = await deleteGeofenceRequestProcessing(friend, geofence.zone_id);
          console.log(res);
          if (res) {
            errorDeleting(geofence, res);
          }
        }
      },
    ]);
  };

  // alert with error message from request
  const errorDeleting = (geofence, error) => {
    Alert.alert(`Геозона ${geofence.name}(${geofence.radius}м) не удалена.`, `${error}`, [
      {
        text: 'ОК',
        style: 'cancel',
      },
    ]);
  };

  const GeofencesArray = () => {
    return (
      textResponse ?
        <TextResponse text={textResponse}/> :
      geofencesListIsReady ? (
        geofencesList.map(geofence => (
          <ListItem
            key={geofence.zone_id}
            leftAvatar={{
              overlayContainerStyle: { backgroundColor: `${geofence.color}` },
            }}
            title={geofence.name}
            subtitle={`${geofence.radius}м`}
            onPress={() => {
              toggleGeofenceItem(geofence);
            }}
            containerStyle={styles.listItemContainer}
            titleStyle={styles.titleStyle}
            subtitleStyle={styles.subtitleStyles}
            pad={10}
            rightIcon={
              <Icon
                type='font-awesome'
                name='trash'
                color='#ff0530'
                size={25}
                onPress={() => {
                  deleteGeofence(props.friend, geofence);
                }}
                iconStyle={{paddingVertical: 5, paddingLeft: 10}}
              />
            }
          />
        ))
      ) : (
        <Loading />
      )
    )
  };

  return createGeofenceIsReady ? (
    <CreateGeofence />
  ) : (
    <View style={styles.container}>
      <ScrollView>
        <View style={{ marginHorizontal: 10, marginBottom: 5 }}>
          <TouchableButton
            btnSize='medium'
            btnAction={showCreateGeofence}
            btnTitle='Создать новую геозону'
            btnBgColor='green'
            iconType='font-awesome'
            iconName='map-pin'
          />
        </View>
        <GeofencesArray />
      </ScrollView>
    </View>
  );
};

export default GeofencesList;

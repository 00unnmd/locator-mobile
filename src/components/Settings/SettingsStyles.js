import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
  },
  title: {
    fontSize: 17,
    fontWeight: 'bold',
  },
  smallTitle: {
    fontSize: 17,
    fontWeight: 'bold',
    alignSelf: 'center',
  },
  twoButtonContainer: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 10,
  },
  spaceBetweenContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  greyLabel: {
    fontSize: 10,
    color: '#aaa',
  },
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  modalView: {
    backgroundColor: 'white',
    borderRadius: 20,
    padding: 15,
    width: '90%',
    minHeight: '80%',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    position: 'relative',
    //justifyContent: 'space-between'
  },
  modalTitle: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  userInfoBlock: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    marginTop: 10
  },
  namingBlock: {
    marginLeft: 10
  },
  usernameTitle: {
    fontSize: 20,
    fontWeight: 'bold'
  },
  phoneTitle: {
    marginTop: 5
  },
  countBlock: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  countItem: {
    textAlign: 'center',
    color: '#aaa'
  },
  littleLabel: {
    fontSize: 10,
    color: '#aaa',
    textAlign: 'center'
  }
});

export default styles;

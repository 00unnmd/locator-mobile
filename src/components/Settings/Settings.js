import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { Modal, Text, TouchableOpacity, View } from 'react-native';
import * as SecureStore from 'expo-secure-store';
import { setTokenConfirmation } from '../../store/authentification/login-control/actions';
import { TouchableHighlight } from 'react-native-gesture-handler';
import { Icon } from 'react-native-elements';
import { Avatar } from 'react-native-elements';

import useSettings from '../../hooks/settings/useSettings';

import TouchableButton from '../TouchableButton/TouchableButton';
import Loading from '../../components/Loading/Loading';
import MetricsItem from '../Metrics/MetricsItem/MetricsItem';

import styles from './SettingsStyles';

const SettingsModal = () => {
  const {
    settings,
    settingsTextResponse,
    settingsIsReady,
    modalVisibility,
    accountSettingsRequestProcessing,
    closeSettingsModal,
    dropSettings,
    logoutClearing
  } = useSettings();
  const dispatch = useDispatch();

  useEffect(() => {
    accountSettingsRequestProcessing();
    console.log('settings: ', settings);
    return () => {
      dropSettings();
    }
  }, []);

  const InfoItem = (props) => {
    return (
      <View>
        <Text style={styles.countItem}>{props.title}</Text>
        <Text style={styles.countItem}>{props.info}</Text>
      </View>
    )
  };

  return (
    <Modal animationType='slide' transparent={true} visible={modalVisibility}>
      <View style={styles.centeredView}>
        <View style={styles.modalView}>
          {settingsIsReady ?
            <React.Fragment>
              <View style={styles.modalTitle}>
                <Text style={styles.smallTitle}>Настройки</Text>
                <TouchableOpacity
                  onPress={() => {
                    closeSettingsModal();
                  }}
                >
                  <Icon type='ionicon' name='ios-close-circle' size={23} color='#aaa' />
                </TouchableOpacity>
              </View>
              <View style={{ flex: 1, justifyContent: 'space-between', marginVertical: 10 }}>
                <View style={styles.userInfoBlock}>
                  <Avatar
                    size='large'
                    rounded
                    source={{ uri: `${settings.avatar}` }}
                    title={settings.name[0] + settings.name[1]}
                  />
                  <View style={styles.namingBlock}>
                    <Text style={styles.usernameTitle}>{settings.username}</Text>
                    <Text>{settings.name}</Text>
                    <Text style={styles.phoneTitle}>{`+${settings.telephone_number}`}</Text>
                  </View>
                </View>
                <View style={styles.countBlock}>
                  <InfoItem title='Друзей' info={settings.friends_count} />
                  <InfoItem title='Геозон' info={settings.geo_zones_count} />
                  <InfoItem title='Отправлено метрик' info={settings.locations_count} />
                </View>
                {settings.last_location &&
                  <React.Fragment>
                    <View>
                      <Text style={{ textAlign: 'center', color: '#aaa', fontSize: 17, marginBottom: 5 }}>Последнее местоположение</Text>
                      <View style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-around', }}>
                        <InfoItem title='Долгота' info={settings.last_location.longitude} />
                        <InfoItem title='Широта' info={settings.last_location.latitude} />
                      </View>
                    </View>
                    <View>
                      <MetricsItem
                        iconName='location-arrow'
                        textContent={settings.last_location.address}
                      />
                      <MetricsItem
                        iconName='history'
                        textContent={settings.last_location.local_time}
                      />
                    </View>
                  </React.Fragment>
                }
              </View>
              <TouchableButton
                btnSize='small'
                btnAction={async () => {
                  await SecureStore.deleteItemAsync('loginToken').then(() => {
                    dispatch(setTokenConfirmation(false));
                  })
                  logoutClearing();
                }}
                btnTitle='Выйти'
                btnBgColor='red'
                iconType='font-awesome'
                iconName='window-close'
              />
              <View style={{ marginTop: 5 }}>
                <Text style={styles.littleLabel}>{`Версия API: ${settings.app_version}`}</Text>
                <Text style={styles.littleLabel}>{`Версия приложения: 1.0 (27.06.2020)`}</Text>
              </View>
            </React.Fragment>
            : <Loading />}
        </View>
      </View>
    </Modal>
  );
}

const Settings = () => {
  const {
    modalVisibility,
    showSettingsModal,
  } = useSettings();

  return (
    <React.Fragment>
      <TouchableHighlight
        underlayColor='transparent'
        style={{
          borderRadius: 10,
        }}
        onPress={() => {
          showSettingsModal();
        }}
      >
        <Icon
          type='font-awesome'
          name='cog'
          size={25}
          color='#aaa'
        />
      </TouchableHighlight>
      {modalVisibility && <SettingsModal />}
    </React.Fragment>
  );
};

export default Settings;

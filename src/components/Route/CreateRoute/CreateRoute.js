import React, { useState, useEffect } from 'react';
import {Text, TextInput, View, TouchableOpacity, Alert} from 'react-native';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import moment from 'moment';

import useCreateRoute from '../../../hooks/route/create/useCreateRoute';

import TouchableButton from "../../TouchableButton/TouchableButton";

import styles from './CreateRouteStyles';

const CreateRoute = (props) => {
  //get current date and yesterday for initial states
  const dayMilliseconds = 24*60*60*1000;
  const currentDate = new Date();
  const yesterday = new Date();
  yesterday.setTime(yesterday.getTime() - dayMilliseconds);

  const [initDate, setInitDate] = useState(yesterday);
  const [finalDate, setFinalDate] = useState(currentDate);
  const [initModal, setInitModal] = useState(false);
  const [finalModal, setFinalModal] = useState(false);
  const {
    closeCreateRoute,
    createRouteRequestProcessing,
    errorCreateRoute,
    textResponse
  } = useCreateRoute();

  const closeModal = () => {
    setInitModal(false);
    setFinalModal(false);
  };

  const handleCreateRoute = () => {
    let parsedInit = {
      day: initDate.getDate(),
      month: initDate.getMonth(),
      year: initDate.getFullYear()
    };
    let parsedFinal = {
      day: finalDate.getDate(),
      month: finalDate.getMonth(),
      year: finalDate.getFullYear()
    };

    if ((parsedInit.year > parsedFinal.year) ||
      (parsedInit.year == parsedFinal.year && parsedInit.month > parsedFinal.month) ||
      (parsedInit.year == parsedFinal.year && parsedInit.month == parsedFinal.month &&
        (parsedInit.day == parsedFinal.day || parsedInit.day > parsedFinal.day))) {
      errorCreateRoute('Неверно указан временной промежуток!');
    }
    else {
      const period = {
        from: moment(initDate).format(),
        to: moment(finalDate).format(),
      };
      createRouteRequestProcessing(props.friend, period);
    }
  };

  return (
    <View style={styles.container}>
      <Text style={styles.title}>Укажите временной промежуток:</Text>
      <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}}>
        <Text>С</Text>
        <TouchableOpacity
          onPress={() => {setInitModal(true)}}
        >
          <TextInput
            style={styles.inputStyle}
            value={moment(initDate).format('DD/MM/YYYY')}
            maxLength={20}
            editable={false}
            pointerEvents='none'
          />
        </TouchableOpacity>
        <DateTimePickerModal
          date={initModal? initDate : finalDate}
          isVisible={initModal || finalModal}
          onConfirm={(date) => {
            initModal? setInitDate(date) : setFinalDate(date);
            closeModal();
          }}
          onCancel={() => {closeModal()}}
          mode='date'
          minimumDate={new Date(2020, 1, 1)}
          maximumDate={currentDate}
        />
        <Text>ПО</Text>
        <TouchableOpacity
          onPress={() => {setFinalModal(true)}}
        >
          <TextInput
            style={styles.inputStyle}
            value={moment(finalDate).format('DD/MM/YYYY')}
            maxLength={20}
            editable={false}
            pointerEvents='none'
          />
        </TouchableOpacity>
      </View>
      <View style={{alignSelf: 'center'}}>
        <Text style={{color: 'red', textAlign: 'center'}}>{textResponse}</Text>
      </View>
      <View style={styles.twoButtonContainer}>
        <TouchableButton
          btnSize='medium'
          btnAction={() => {
            handleCreateRoute();
          }}
          btnTitle='Построить'
          btnBgColor='green'
          iconType='font-awesome'
          iconName='plus'
        />
        <TouchableButton
          btnSize='medium'
          btnAction={closeCreateRoute}
          btnTitle='Отмена'
          btnBgColor='red'
          iconType='font-awesome'
          iconName='ban'
        />
      </View>
    </View>
  );
};

export default CreateRoute;
import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
  },
  title: {
    fontSize: 17,
    fontWeight: 'bold',
  },
  twoButtonContainer: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 10,
  },
  inputStyle: {
    marginVertical: 10,
    paddingHorizontal: 5,
    paddingVertical: 5,
    minHeight: 36,
    borderWidth: 1,
    borderColor: '#dcdce1',
    borderRadius: 9,
    backgroundColor: '#dcdce1',
    fontSize: 18,
  },
});

export default styles;

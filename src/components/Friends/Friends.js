import React from 'react';

import useFriendsList from '../../hooks/friends/friendsList/useFriendsList';
import useSearchFriends from '../../hooks/friends/searchFriends/useSearchFriends';
import useFriendInfo from '../../hooks/friends/friendInfo/useFriendInfo';

import FriendListDrawer from './FriendsList/FriendsListDrawer/FriendsListDrawer';
import SearchDrawer from '../Search/SearchDrawer/SearchDrawer';
import FriendInfoDrawer from './FriendInfo/FriendInfoDrawer/FriendInfoDrawer';

const Friends = () => {
  const { friendsDrawerVisible } = useFriendsList();
  const { searchListIsVisible } = useSearchFriends();
  const { userInfoIsVisible } = useFriendInfo();

  return userInfoIsVisible ? (
    <FriendInfoDrawer />
  ) : searchListIsVisible ? (
    <SearchDrawer />
  ) : friendsDrawerVisible ? (
    <FriendListDrawer />
  ) : null;
};

export default Friends;

import React, { useEffect } from 'react';

import useFriendsList from '../../../hooks/friends/friendsList/useFriendsList';
import useFriendInfo from '../../../hooks/friends/friendInfo/useFriendInfo';

//components
import FriendsItem from '../FriendsItem/FriendsItem';
import Loading from '../../Loading/Loading';
import TextResponse from '../../TextResponse/TextResponse';

const FriendsList = () => {
  const {
    friendsList,
    friendsListIsReady,
    listTextResponse,
    friendsListRequestProcessing,
    closeFriendListDrawer
  } = useFriendsList();
  const { showUserInfo } = useFriendInfo();

  const showFriendInfo = (user) => {
    showUserInfo(user);
    closeFriendListDrawer();
  };

  const fetchData = async () => {
    await friendsListRequestProcessing();
  };

  useEffect(() => {
    fetchData();
  }, []);

  return listTextResponse ? (
    <TextResponse text={listTextResponse} />
  ) : friendsListIsReady ? (
    <FriendsItem array={friendsList} onItemClick={showFriendInfo} />
  ) : (
    <Loading />
  );
};

export default FriendsList;

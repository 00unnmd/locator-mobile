import React from 'react';
import { Dimensions } from 'react-native';

import useFriendsList from '../../../../hooks/friends/friendsList/useFriendsList';
import useSearchFriends from '../../../../hooks/friends/searchFriends/useSearchFriends';

//components
import DrawerWrap from '../../../Drawers/DrawerWrap/DrawerWrap';
import FriendsList from '../FriendsList';

const windowHeight = Dimensions.get('window').height; //need for calculate height of drawer & downDisplay

const FriendsListDrawer = () => {
  const { closeFriendListDrawer } = useFriendsList();
  const { toggleSearchList } = useSearchFriends();

  //показать search drawer по клику на иконку поиска
  const showSearchDrawer = () => {
    toggleSearchList(true);
    closeFriendListDrawer();
  };

  return (
    <DrawerWrap
      containerHeight={windowHeight - 20}
      downDisplay={(windowHeight - 20)/ 2}
      startUp={false}
      icon='ios-search'
      title='Друзья'
      iconAction={showSearchDrawer}
      stylesContainer={{ marginHorizontal: 0 }}
      content={<FriendsList />}
    />
  );
};

export default FriendsListDrawer;

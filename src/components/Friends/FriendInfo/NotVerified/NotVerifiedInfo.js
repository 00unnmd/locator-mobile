import React from 'react';

import useAddFriend from '../../../../hooks/friends/addFriend/useAddFriend';
import useReplyFriend from '../../../../hooks/friends/replyFriend/useReplyFriend';

import TouchableButton from '../../../TouchableButton/TouchableButton';
import styles from './NotVerifiedInfoStyles';
import { View } from 'react-native';

const NotVerifiedInfo = props => {
  const { addFriendTextResponse, addFriendRequestProcessing } = useAddFriend();
  const { applyFriendRequestProcessing, declineFriendRequestProcessing } = useReplyFriend();

  return props.user.verified === undefined ? (
    <View style={{ width: '100%' }}>
      <TouchableButton
        data={props.user.id}
        btnSize='medium'
        btnAction={addFriendRequestProcessing}
        btnTitle={addFriendTextResponse}
        btnBgColor='green'
        iconType='font-awesome'
        iconName='user-plus'
      />
    </View>
  ) : (
    props.user.allow_to_confirm && (
      <View style={styles.twoButtonContainer}>
        <TouchableButton
          data={props.user}
          btnSize='large'
          btnAction={applyFriendRequestProcessing}
          btnTitle='Принять'
          btnBgColor='green'
          iconType='font-awesome'
          iconName='plus-square'
        />
        <TouchableButton
          data={props.user}
          btnSize='large'
          btnAction={declineFriendRequestProcessing}
          btnTitle='Отклонить'
          btnBgColor='red'
          iconType='font-awesome'
          iconName='minus-square'
        />
      </View>
    )
  );
};

export default NotVerifiedInfo;

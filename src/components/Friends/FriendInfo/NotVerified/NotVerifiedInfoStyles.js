import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  twoButtonContainer: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
});

export default styles;

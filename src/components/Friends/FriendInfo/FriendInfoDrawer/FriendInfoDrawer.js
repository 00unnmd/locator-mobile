import React from 'react';
import { Dimensions } from 'react-native';

import useFriendsList from '../../../../hooks/friends/friendsList/useFriendsList';
import useFriendInfo from '../../../../hooks/friends/friendInfo/useFriendInfo';
import useGeofencesList from '../../../../hooks/geofences/list/useGeofencesList';
import useCreateRoute from '../../../../hooks/route/create/useCreateRoute';

import DrawerWrap from '../../../Drawers/DrawerWrap/DrawerWrap';
import FriendInfo from '../FriendInfo';

const windowHeight = Dimensions.get('window').height; //need for calculate height of drawer & downDisplay

const FriendInfoDrawer = () => {
  const { showFriendListDrawer } = useFriendsList();
  const { user, clearUserInfo } = useFriendInfo();
  const { geofencesListIsVisible, closeGeofenceList } = useGeofencesList();
  const { createRouteIsVisible, closeCreateRoute } = useCreateRoute();

  const closeFriendInfo = () => {
    clearUserInfo();
    showFriendListDrawer();
  };

  return (
    <DrawerWrap
      containerHeight={windowHeight - 20}
      downDisplay={(windowHeight - 20) / 2}
      startUp={false}
      icon='ios-close-circle'
      title={user.username}
      subTitle={`${user.name && user.name} ${user.status && user.status}`}
      iconAction={geofencesListIsVisible ? closeGeofenceList :
        createRouteIsVisible ? closeCreateRoute : closeFriendInfo}
      stylesContainer={{ marginHorizontal: 0 }}
      content={<FriendInfo />}
    />
  );
};

export default FriendInfoDrawer;

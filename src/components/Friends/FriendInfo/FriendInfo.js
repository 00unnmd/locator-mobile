import React, { useEffect } from 'react';
import { View } from 'react-native';

import useFriendsList from '../../../hooks/friends/friendsList/useFriendsList';
import useFriendInfo from '../../../hooks/friends/friendInfo/useFriendInfo';

import VerifiedInfo from './Verified/VerifiedInfo';
import NotVerifiedInfo from './NotVerified/NotVerifiedInfo';

const FriendInfo = () => {
  const { friendsListRequestProcessing } = useFriendsList();
  const { user } = useFriendInfo();

  const fetchData = async () => {
    await friendsListRequestProcessing();
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <View style={{ marginHorizontal: 10, marginTop: 5, flex: 1 }}>
      {user.verified ? <VerifiedInfo user={user} /> : <NotVerifiedInfo user={user} />}
    </View>
  );
};

export default FriendInfo;

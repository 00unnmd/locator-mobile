import React, { useEffect } from 'react';
import { Linking, Alert, View } from 'react-native';

import useFriendInfo from '../../../../hooks/friends/friendInfo/useFriendInfo';
import useDeleteFriend from '../../../../hooks/friends/deleteFriend/useDeleteFriend';
import useGeofencesList from '../../../../hooks/geofences/list/useGeofencesList';
import useCreateRoute from '../../../../hooks/route/create/useCreateRoute';

import subscribeChannel from '../../../../helpers/ActionCable/channelSubscriber';
import unsubscribeChannel from '../../../../helpers/ActionCable/channelUnsubscriber';

import GeofencesList from '../../../Geofences/GeofencesList/GeofencesList';
import CreateRoute from '../../../Route/CreateRoute/CreateRoute';
import MetricsItem from '../../../Metrics/MetricsItem/MetricsItem';
import TouchableButton from '../../../TouchableButton/TouchableButton';
import TextResponse from '../../../TextResponse/TextResponse';
import styles from './VerifiedInfoStyles';

const VerifiedInfo = props => {
  const {
    friend,
    friendInfoIsReady,
    infoTextResponse,
    friendInfoRequestProcessing,
  } = useFriendInfo();
  const { deleteFriendRequestProcessing } = useDeleteFriend();
  const { geofencesListIsVisible, showGeofenceList } = useGeofencesList();
  const { createRouteIsVisible, showCreateRoute } = useCreateRoute();

  //получение информации о друге
  const fetchFriendInfo = async () => {
    await friendInfoRequestProcessing(props.user);
  };
  useEffect(() => {
    fetchFriendInfo();
  }, []);
  //действия с сокетом
  useEffect(() => {
    if (friendInfoIsReady) {
      console.log('friend: ', friend);

      subscribeChannel(friend.id);
      return () => {
        unsubscribeChannel();
      };
    }
  }, [friendInfoIsReady]);

  const MetricsInfo = () => {
    return (
      <View style={{ marginBottom: 10 }}>
        <MetricsItem iconName='location-arrow' textContent={friend.metrics.location.address} />
        <View style={[{ display: 'flex', flexDirection: 'row', justifyContent: 'space-between' }]}>
          <MetricsItem iconName='history' textContent={friend.parsed.parsedTime} />
          <MetricsItem
            iconName={friend.parsed.parsedBattery}
            textContent={`${friend.metrics.battery}% `}
            reversed
          />
        </View>
      </View>
    );
  };

  const InfoBlock = () => {
    const callAction = friend => {
      Linking.openURL(`tel:${friend.telephone_number}`);
    };
    const deleteAction = friend => {
      Alert.alert(`Вы действительно хотите удалить пользователя ${friend.username}?`, '', [
        {
          text: 'Отмена',
          style: 'cancel',
        },
        {
          text: 'Удалить',
          style: 'destructive',
          onPress: () => deleteFriendRequestProcessing(friend.id),
        },
      ]);
    };

    return (
      <React.Fragment>
        {friendInfoIsReady && <MetricsInfo />}
        <View style={styles.twoButtonContainer}>
          <TouchableButton
            data={friend}
            btnSize='medium'
            btnAction={callAction}
            btnTitle='Позвонить'
            btnBgColor='green'
            iconType='font-awesome'
            iconName='phone'
            disabled={!friendInfoIsReady}
          />
          <TouchableButton
            data={friend}
            btnSize='medium'
            btnAction={deleteAction}
            btnTitle='Удалить'
            btnBgColor='red'
            iconType='font-awesome'
            iconName='trash'
            disabled={!friendInfoIsReady}
          />
        </View>
        <View style={{ marginTop: 10 }}>
          <TouchableButton
            btnSize='medium'
            btnAction={showGeofenceList}
            btnTitle='Список геозон'
            btnBgColor='neutral'
            iconType='font-awesome'
            iconName='street-view'
            disabled={!friendInfoIsReady}
          />
        </View>
        <View style={{ marginTop: 10 }}>
          <TouchableButton
            btnSize='medium'
            btnAction={showCreateRoute}
            btnTitle='Маршруты'
            btnBgColor='neutral'
            iconType='font-awesome'
            iconName='road'
            disabled={!friendInfoIsReady}
          />
        </View>
      </React.Fragment>
    );
  };

  return infoTextResponse ? (
    <TextResponse text={infoTextResponse} />
  ) : geofencesListIsVisible ? (
    <GeofencesList friend={friend} />
  ) : createRouteIsVisible ? (
    <CreateRoute friend={friend}/>
  ) : <InfoBlock />
};

export default VerifiedInfo;

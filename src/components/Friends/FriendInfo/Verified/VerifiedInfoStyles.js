import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  metricsInfo: {
    display: 'flex',
    flexDirection: 'row',
    marginBottom: 5,
  },
  addressText: {
    paddingLeft: 5,
    fontSize: 12,
    color: '#aaa',
  },
  twoButtonContainer: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
});

export default styles;

import React from 'react';

import { ScrollView } from 'react-native';
import { ListItem } from 'react-native-elements';

import styles from './FriendsItemStyles';

const FriendsItem = props => {
  const list = props.array;

  return (
    <ScrollView>
      {list.map(user => (
        <ListItem
          key={user.id}
          leftAvatar={{
            title: user.name[0] + user.name[1],
            placeholderStyle: styles.placeholderStyle,
            source: { uri: user.avatar },
          }}
          title={user.username}
          subtitle={user.status ? user.status : user.name}
          chevron
          onPress={() => {
            props.onItemClick(user);
          }}
          containerStyle={styles.containerStyle}
          titleStyle={styles.titleStyle}
          subtitleStyle={styles.subtitleStyles}
          pad={10}
        />
      ))}
    </ScrollView>
  );
};

export default FriendsItem;

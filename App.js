import React from 'react';
import { Provider } from 'react-redux';

import store from './src/store/store';
import { LoginControl } from './src/components/Authentification/LoginControl/LoginControl';

const App = () => {
  return (
    <Provider store={store}>
      <LoginControl />
    </Provider>
  );
};

export default App;
